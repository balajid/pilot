import pandas as pd

def get_matrix():
    from app import db
    from app.model import police
    m = police.Police.get_matrix()
    return pd.read_sql(m.statement, db.engine)

def get_demographic_matrix():
    from app import db
    from app.model import demographic
    m = demographic.Demographics.get_matrix()
    return pd.read_sql(m.statement, db.engine)


def get_incident_matrix():
    from app import db
    from app.model import incidents
    m=incidents.Incidents.get_matrix()
    return pd.read_sql(m.statement,db.engine)

if __name__ == '__main__':
    m = get_demographic_matrix()
    print(m.describe())