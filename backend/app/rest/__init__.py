import json
import os

from flask import Blueprint, request
from flask_restful import Resource, Api
import pandas as pd
import index_creator
from app.model.police import Police
from app.model.demographic import Demographics
from app.model.incidents import Incidents
from app.model.indices import Indices
from app.model.indicesprop import Indicesprop
from app import db
import math
import numpy as np
import pprint
from ml.interpretation import interpretation
from ml.interpretation import regression1
from ml.interpretation import regression2
from ml.interpretation import regression3

def norm_0_1(x):
    x_norm = (x - x.min()) / (x.max() - x.min())
    return x_norm

def my_pca(x, kfactor=False):
    cov = np.dot(x.T, x)
    eigvals, eigvecs = np.linalg.eig(cov)
    alpha = eigvals / eigvals.sum()
    if kfactor:
        alpha = alpha[alpha > 0.01]
    k = alpha.shape[0]
    x_rotate = np.dot(x, eigvecs[:, :k])
    I = norm_0_1(np.dot(x_rotate, alpha))

    return x_rotate,I

class Index(Resource):
        def get(self):
            distinct = Police.query.with_entities(Police.ucr_description, Police.category, Police.subcategory).distinct().all()
            ret = {}
            for d in distinct:
                if not d[1] in ret: ret[d[1]] = {}
                if d[2] not in ret[d[1]]: ret[d[1]][d[2]] = []
                ret[d[1]][d[2]].append(d[0])

            inQuery = Incidents.query.with_entities(Incidents.incident_cat).distinct().all()

            incidentArr=[]
            for initer in inQuery:
                incidentArr.append(initer)
            inc=dict()
            inc['incident']=incidentArr
            print(incidentArr)

            demQuery = Demographics.query.with_entities(Demographics.attribute).distinct().all()
            counter=0
            demj=dict()
            maxx=0
            for iter1 in demQuery:
                temp=str(iter1).split('|')
                temp[0]=temp[0].replace('("','')
                t=temp[0].replace('("','')
                t=t.lstrip('(\'')
                if(str(t) == "Mother tongue for the total population excluding institutional residents - 100% data" or str(t)=="Private households by number of persons per room - 25% sample data"):
                    continue
                length=len(temp)
                if(length>maxx):
                    maxx=length
                tdict=[]
                for i in range(0,length):
                    if(i == length-1):
                        tval = bytes(temp[i],'utf-8').decode('unicode-escape').replace('\',)','')
                        if(tval.endswith('",)')):
                            tval=tval.replace('",)','')
                        if(i==0):
                            td2=demj
                            tval=tval.replace('(\'','')
                        else:
                            td2=tdict.pop()
                        if('none' not in td2):
                            temp2=[]
                            temp2.append(tval)
                            td2['none']=temp2
                        else:
                            td2['none'].append(tval)

                    if (i == 0):
                        if(i==length-1):
                            continue
                        temp[i] = temp[i].replace('(\'', '')
                        if (temp[i] not in demj):
                            demj[temp[i]]={}
                        tdict.append(demj[temp[i]])

                    if(i==length-2):

                        if(i==0):
                            continue
                        td3=tdict.pop()
                        if(temp[i] not in td3):
                            td3[temp[i]]={}
                        tdict.append(td3[temp[i]])

                    if(i>0 and i<length-2):
                        td=tdict.pop()
                        #print(td)
                        if(temp[i] not in td):
                            td[temp[i]]={}
                        tdict.append(td[temp[i]])
                counter=counter+1

            #print(counter)
            pp = pprint.PrettyPrinter(indent=1)
            for k in demj.keys():
                print(k)
            #print(demj['none'])
            # pp.pprint(demj['Total population aged 15 years and over by work activity during the reference year - 25% sample data'])
            return {'dem':demj,'ret':ret,'inc':inc}

class PcaCrime(Resource):
    def get(self):
        args = request.args
        d1 = args['d1']
        d2 = args['d2']
        valX = []
        wx = []
        sx = []
        maxx = []
        argSize = len(request.args)
        for a in args.keys():
            if (a != 'd1' and a != 'd2'):
                valX.append(a)
                max_index = Police.query.filter_by(ucr_description=a).first().severity_weight
                if max_index is None or 0: max_index = 1
                maxx.append(int(math.log(max_index)))
        counter = argSize - 2
        for b in args.values():
            if (counter > 0):
                if (int(b) < 1):
                    sx.append(-1)
                    wx.append(math.fabs(int(b)))
                else:
                    sx.append(1)
                    wx.append(b)
            counter = counter - 1
        indVar = dict()
        vlen = len(valX)
        tempP = []
        for i in range(0, vlen):
            tempd = dict()
            tempd['ind'] = valX[i]
            tempd['weight'] = wx[i]
            tempd['maxweight'] = maxx[i]
            tempd['minweight'] = 0
            tempd['sign'] = sx[i]
            tempP.append(tempd)
        indVar['propor'] = tempP

        entries = Police.get_sortedmatrix(d1, d2)
        dentry = Demographics.get_dauID()
        dauID = pd.read_sql(dentry.statement, db.engine)

        df = pd.read_sql(entries.statement, db.engine)

        df = df[df.ucr_description.isin(valX)]
        count = df.pivot(index='dauid', columns='ucr_description', values='count_1')
        # sev = df.pivot(index='dauid', columns='ucr_description', values='severity_weight')
        count = count.fillna(0)
        # sev = sev.fillna(0)
        # args2 = ([float(a) for a in args.values()] * sev)
        weight = []
        fmax = []
        sumW = 0
        citer = 0
        counter = argSize - 2
        dsize = count.values.shape[0]
        newcount = []
        for iter in args.keys():
            # if (float(iter) < 0):
            #     weight.append(-1 * math.log(math.fabs(float(iter))))
            # elif (float(iter) >= 0 and float(iter) <= 1):
            #     weight.append(float(iter))
            # else:
            #     weight.append(math.log(math.fabs(float(iter))))
            # weight.append(float(iter))
            # fmax.append(count.values[:, citer:citer + 1].max())
            if(counter>0):

                weight.append(float(args[iter]))
                if (iter in count):
                    fmax.append(count[iter].values.max())
                    newcount.append(count[iter].values)
                    # fmax.append(count.values[:, citer:citer+1].max())
                else:
                    fmax.append(1)
                    newcount.append(np.zeros(dsize))
                citer = citer + 1
            counter=counter-1

        newcount = np.asarray(newcount)
        # newcount = newcount.transpose()
        w=np.asarray(wx).astype(float)
        count_x=newcount.T * w
        if(count_x.shape[0]==0):
            count_pca=[0]
            respca=w*0
        else:
            count_pca,respca=my_pca(count_x)
            count_pca=np.nan_to_num(count_pca)
            respca=np.nan_to_num(respca)
            count_pca=(count_pca-count_pca.min())/(count_pca.max()-count_pca.min())
        indicator = dict()
        for did, vl in zip(count.index, count_pca):
            indicator[did] = vl
        result2=dict()
        for did,value in zip(count.index,respca):
            result2[did]=value
        filler = np.zeros(len(weight))

        for dess in dauID.values:
            if dess[0] not in indicator.keys():
                indicator[dess[0]] = filler
                result2[dess[0]]=0
        sums = []
        result2max = max(result2.values())
        result2min = min(result2.values())
        resdiff = result2max - result2min
        indDic = dict()
        resu3 = dict()
        for key in result2.keys():
            resu3[key] = result2[key]
            indDic[str(key)] = resu3[key]
        for dauid in resu3.keys():
            sums.append(dict(DAUID=str(dauid), Index=resu3[dauid], X=valX, Y=indicator[dauid].tolist()))
        indVar['index'] = indDic

        resu4 = dict()
        for key2 in resu3.keys():
            resu4[str(key2)] = resu3[key2]
        ind = {}
        for did in indicator.keys():
            temp = str(did)
            ind[temp] = indicator[did].tolist()
        if (os.path.exists('index.json') == True):
            with open('index.json') as json_file:
                indexD = json.load(json_file)
                indexD['indices'].append(indVar)
        else:
            indexD = dict()
            indexD['indices'] = []
            indexD['indices'].append(indVar)
        pl = Indices.get_next()
        indices = pd.read_sql(pl.statement, db.engine)
        indices = indices.fillna(0)
        last_ind = indices.values.flatten()[0]
        last_ind = int(last_ind) + 1
        iname = 'index' + str(last_ind)
        for ind in indVar['index']:
            ind2 = int(ind)
            d = Indices(index_name=iname, index_no=last_ind, dauid=ind2, index=indVar['index'][ind])
            db.session.add(d)
        db.session.commit()
        for propor in indVar['propor']:
            d2 = Indicesprop(index_name=iname, index_no=last_ind, variable=propor['ind'], weight=propor['weight'],
                             sign=propor['sign'], maxweight=propor['maxweight'], minweight=propor['minweight'])
            db.session.add(d2)
        db.session.commit()
        db.session.close()
        print(len(indexD['indices']))
        # with open('index.json', 'w') as json_file:
        #     json.dump(indexD, json_file)
        return dict(sums=sums, resu2=resu4, indic=ind, valX=valX)

class Severity(Resource):
    def get(self):
        max_index = Police.query.filter_by(ucr_description=request.args['param2[]']).first().severity_weight
        if max_index is None or 0: max_index = 1
        return {'min':0, 'max':int(math.log(max_index))}

class DemCreation(Resource):
    def get(self):
        args2=request.args
        valX=[]
        for a in args2.keys():
            valX.append(a)
        entries2=Demographics.get_matrix()
        dentry2 = Demographics.get_dauID()
        dauID2 = pd.read_sql(dentry2.statement, db.engine)
        demEntry=pd.read_sql(entries2.statement,db.engine)
        print(args2.keys())
        demEntry = demEntry[demEntry.attribute.isin(args2.keys())]
        print(demEntry)
        count2=demEntry.pivot(index='dauid', columns='attribute',values='total')
        count2 = count2.fillna(0)
        weight2 = []
        fmax2=[]
        sweight=0
        citer=0
        for iter in args2.values():
            # if (float(iter) < 0):
            #     weight2.append(-1 * math.log(math.fabs(float(iter))))
            # elif (float(iter) >= 0 and float(iter) <= 1):
            #     weight2.append(float(iter))
            # else:
            #     weight2.append(math.log(math.fabs(float(iter))))
            weight2.append(float(iter))
            fmax2.append(count2.values[:, citer:citer + 1].max())
            sweight=sweight+math.fabs(float(iter))
            citer=citer+1
        print(weight2)
        print(sweight)
        print(count2)
        fmax3 = np.multiply(fmax2, sweight)
        indicator = dict()
        for did, vl in zip(count2.index, count2.values):
            indicator[did] = (vl * weight2)/fmax3
        filler = np.zeros(len(weight2))
        for dess2 in dauID2.values:
            if dess2[0] not in indicator.keys():
                indicator[dess2[0]] = filler

        result2 = indicator.copy()
        for diss in indicator.keys():
            result2[diss] = sum(indicator[diss])
        sums = []
        result2max = max(result2.values())
        result2min = min(result2.values())
        resdiff = result2max - result2min
        resu3 = dict()
        for key in result2.keys():
            resu3[key] = result2[key]

        for dauid in resu3.keys():
            sums.append(dict(DAUID=str(dauid), Index=resu3[dauid], X=valX, Y=indicator[dauid].tolist()))
        resu4=dict()
        for key2 in resu3.keys():
            resu4[str(key2)]=resu3[key2]
        ind = {}
        for did in indicator.keys():
            temp = str(did)
            ind[temp] = indicator[did].tolist()
        return dict(sums=sums, resu2=resu4, indic=ind, valX=valX)
class IndexCreation(Resource):
        def get(self):
            args = request.args
            d1 = args['d1']
            d2 = args['d2']
            valX=[]
            wx=[]
            sx=[]
            maxx=[]
            argSize=len(request.args)
            for a in args.keys():
                if(a!='d1' and a!='d2'):
                    valX.append(a)
                    max_index = Police.query.filter_by(ucr_description=a).first().severity_weight
                    if max_index is None or 0: max_index = 1
                    maxx.append(int(math.log(max_index)))
            counter=argSize-2
            for b in args.values():
                if(counter>0):
                    if(int(b)<1):
                        sx.append(-1)
                        wx.append(math.fabs(int(b)))
                    else:
                        sx.append(1)
                        wx.append(b)
                counter=counter-1
            indVar=dict()
            vlen=len(valX)
            tempP=[]
            for i in range(0,vlen):
                tempd=dict()
                tempd['ind']=valX[i]
                tempd['weight']=wx[i]
                tempd['maxweight']=maxx[i]
                tempd['minweight']=0
                tempd['sign']=sx[i]
                tempP.append(tempd)
            indVar['propor']=tempP

            entries = Police.get_sortedmatrix(d1,d2)
            dentry = Demographics.get_dauID()
            dauID=pd.read_sql(dentry.statement,db.engine)

            df = pd.read_sql(entries.statement, db.engine)

            df = df[df.ucr_description.isin(valX)]
            count = df.pivot(index='dauid', columns='ucr_description', values='count_1')
            #sev = df.pivot(index='dauid', columns='ucr_description', values='severity_weight')
            count = count.fillna(0)
            #sev = sev.fillna(0)
            # args2 = ([float(a) for a in args.values()] * sev)
            weight=[]
            fmax=[]
            sumW=0
            citer=0
            counter=argSize-2
            dsize=count.values.shape[0]
            newcount=[]
            for iter in args.keys():
                if(counter>0):
                    # if (float(iter) < 0):
                    #     weight.append(-1 * math.log(math.fabs(float(iter))))
                    # elif (float(iter) >= 0 and float(iter) <= 1):
                    #     weight.append(float(iter))
                    # else:
                    #     weight.append(math.log(math.fabs(float(iter))))
                    weight.append(float(args[iter]))
                    if(iter in count):
                        fmax.append(count[iter].values.max())
                        newcount.append(count[iter].values)
                        #fmax.append(count.values[:, citer:citer+1].max())
                    else:
                        fmax.append(1)
                        newcount.append(np.zeros(dsize))
                    citer=citer+1
                    sumW=sumW+math.fabs(float(args[iter]))
                counter=counter-1
            newcount=np.asarray(newcount)
            newcount=newcount.transpose()
            print(weight,sumW)
            print(count.values)
            fmax=np.multiply(fmax,sumW)
            print(fmax)
            indicator=dict()
            for did,vl in zip(count.index,newcount):
                indicator[did]=(vl*weight)/fmax
            filler=np.zeros(len(weight))
            for dess in dauID.values:
                if dess[0] not in indicator.keys():
                    indicator[dess[0]]=filler

            result2=indicator.copy()
            for diss in indicator.keys():
                result2[diss]=sum(indicator[diss])
            sums = []
            result2max = max(result2.values())
            result2min = min(result2.values())
            resdiff =result2max-result2min
            indDic=dict()
            resu3 = dict()
            for key in result2.keys():
                resu3[key]=result2[key]
                indDic[str(key)]=resu3[key]
            for dauid in resu3.keys():
                sums.append(dict(DAUID=str(dauid), Index=resu3[dauid], X=valX, Y=indicator[dauid].tolist()))
            indVar['index']=indDic

            resu4=dict()
            for key2 in resu3.keys():
                resu4[str(key2)]=resu3[key2]
            ind={}
            for did in indicator.keys():
                temp=str(did)
                ind[temp]=indicator[did].tolist()
            if(os.path.exists('index.json')==True):
                with open('index.json') as json_file:
                    indexD=json.load(json_file)
                    indexD['indices'].append(indVar)
            else:
                indexD=dict()
                indexD['indices']=[]
                indexD['indices'].append(indVar)
            pl = Indices.get_next()
            indices = pd.read_sql(pl.statement, db.engine)
            indices = indices.fillna(0)
            last_ind = indices.values.flatten()[0]
            last_ind=int(last_ind)+1
            iname = 'index' + str(last_ind)
            for ind in indVar['index']:
                ind2=int(ind)
                d=Indices(index_name=iname,index_no=last_ind,dauid=ind2,index=indVar['index'][ind])
                db.session.add(d)
            db.session.commit()
            for propor in indVar['propor']:
                d2=Indicesprop(index_name=iname,index_no=last_ind,variable=propor['ind'],weight=propor['weight'],sign=propor['sign'],maxweight=propor['maxweight'],minweight=propor['minweight'])
                db.session.add(d2)
            db.session.commit()
            db.session.close()
            print(len(indexD['indices']))
            # with open('index.json', 'w') as json_file:
            #     json.dump(indexD, json_file)
            return dict(sums=sums,resu2=resu4, indic=ind,valX=valX)

class Storage:
    intermodel = None
    interdata = None
    dfindices = None
    pdindices = None
    @staticmethod
    def set(model,data,dind,pdind):
        Storage.intermodel=model
        Storage.interdata = data
        Storage.dfindices=dind
        Storage.pdindices=pdind

class Interpreter3(Resource):

    def get(self):

        args = request.args
        vars = []
        for i in args.keys():
            vars.append(i)
        if (os.path.exists('index.json') == True):
            with open('index.json') as json_file:
                indexD = json.load(json_file)
                ilen = len(indexD['indices'])
                indices = indexD['indices'][ilen - 1]['index']
                indices = pd.DataFrame.from_dict(indices, orient='index')
                dids = list(map(int, indexD['indices'][ilen - 1]['index'].keys()))
                rmodel, rdata, rindices, pindices, rscore = regression3(indices, vars, 5, 5)
                dentry = Demographics.get_dauID()
                dauID = pd.read_sql(dentry.statement, db.engine)
                for dess in dauID.values:
                    if dess[0] not in rdata.index.unique():
                        tdict = dict()
                        for var in vars:
                            tdict[var] = float(0)
                        row = pd.Series(tdict, name=dess[0])
                        rdata = rdata.append(row)
                        pindices = np.append(pindices, float(0))
                rindex = indices.values.flatten()
                tres = pindices - rindex
                pind = tres.tolist()
                tl1 = rdata.index.astype('str').to_list()
                tlen = len(tl1)
                result = dict()
                sums = []
                for iter in range(0, tlen):
                    result[tl1[iter]] = pind[iter]
                    sums.append(dict(DAUID=tl1[iter], Index=pind[iter]))
                Storage.set(rmodel, rdata, indices, pindices)
                print(indices, pind)

        return dict(sums=sums, pred=result)

class Interpreter2(Resource):

        def get(self):

            args=request.args
            vars=[]
            for i in args.keys():
                vars.append(i)

            pl = Indices.get_next()
            indices = pd.read_sql(pl.statement, db.engine)
            indices = indices.fillna(0)
            last_ind = int(indices.values.flatten()[0])

            pl2 = Indices.query.filter_by(index_no=last_ind)
            indices2 = pd.read_sql(pl2.statement, db.engine)
            indices = indices2.pivot(index='dauid', columns='index_name', values='index')
            dids=list(set(indices.index.unique()))
            rmodel, rdata, rindices,pindices,rscore=regression2(indices,vars,5,5)
            dentry = Demographics.get_dauID()
            dauID = pd.read_sql(dentry.statement, db.engine)
            for dess in dauID.values:
                if dess[0] not in rdata.index.unique():
                    tdict=dict()
                    for var in vars:
                        tdict[var]=float(0)
                    row = pd.Series(tdict, name=dess[0])
                    rdata = rdata.append(row)
                    pindices=np.append(pindices,float(0))
            rindex=indices.values.flatten()
            tres = (pindices-rindex)/2
            pind=tres.tolist()
            tl1 = rdata.index.astype('str').to_list()
            tlen=len(tl1)
            result=dict()
            sums=[]
            for iter in range(0,tlen):
                result[tl1[iter]]=pind[iter]
                sums.append(dict(DAUID=tl1[iter], Index=pind[iter]))
            Storage.set(rmodel,rdata,indices,pindices)
            print(indices,pind)

            return dict(sums=sums,pred=result)


class Interpreter1(Resource):

    def get(self):

        args = request.args
        vars = []
        for i in args.keys():
            vars.append(i)

        pl = Indices.get_next()
        indices = pd.read_sql(pl.statement, db.engine)
        indices = indices.fillna(0)
        last_ind = int(indices.values.flatten()[0])

        pl2 = Indices.query.filter_by(index_no=last_ind)
        indices2 = pd.read_sql(pl2.statement, db.engine)
        indices = indices2.pivot(index='dauid', columns='index_name', values='index')
        dids = list(set(indices.index.unique()))

        rmodel, rdata, rindices, pindices, rscore = regression1(indices, vars, 5, 5)
        dentry = Demographics.get_dauID()
        dauID = pd.read_sql(dentry.statement, db.engine)
        for dess in dauID.values:
            if dess[0] not in rdata.index.unique():
                tdict = dict()
                for var in vars:
                    tdict[var] = float(0)
                row = pd.Series(tdict, name=dess[0])
                rdata = rdata.append(row)
                pindices = np.append(pindices, float(0))
        rindex = indices.values.flatten()
        tres = (pindices - rindex)/2
        pind = tres.tolist()
        tl1 = rdata.index.astype('str').to_list()
        tlen = len(tl1)
        result = dict()
        sums = []
        for iter in range(0, tlen):
            result[tl1[iter]] = pind[iter]
            sums.append(dict(DAUID=tl1[iter], Index=pind[iter]))
        Storage.set(rmodel, rdata, indices, pindices)
        print(indices, pind)

        return dict(sums=sums, pred=result)

class Explainer(Resource):
    def get(self):

        args1=request.args
        dauid=int(args1['DAUID'])
        elist, idl, pl = interpretation(dauid, Storage.dfindices, Storage.pdindices, Storage.intermodel, Storage.interdata)
        rlist=[]
        for i in elist:
            rlist.append((i[0].split('|')[-1].split('<=')[0].split('>')[0], i[1]))

        return dict(exp=rlist)

        def post(self):
            data = json.loads(request.data)
            return index_creator.create_index()

class IncCreation(Resource):
    def get(self):
        args3=request.args
        valX = []
        sx=[]
        wx=[]
        for a in args3.keys():
            valX.append(a)
        for b in args3.values():
            if (int(b) < 1):
                sx.append(-1)
                wx.append(math.fabs(int(b)))
            else:
                sx.append(1)
                wx.append(b)
        indVar = dict()
        vlen = len(valX)
        tempP = []
        for i in range(0, vlen):
            tempd = dict()
            tempd['ind'] = valX[i]
            tempd['weight'] = wx[i]
            tempd['maxweight'] = math.fabs(100)
            tempd['minweight'] = 0
            tempd['sign'] = sx[i]
            tempP.append(tempd)
        indVar['propor'] = tempP
        entries2 = Incidents.get_matrix()
        dentry2 = Demographics.get_dauID()
        dauID2 = pd.read_sql(dentry2.statement, db.engine)
        inEntry = pd.read_sql(entries2.statement, db.engine)
        print(args3.keys())
        inEntry = inEntry[inEntry.incident_cat.isin(args3.keys())]
        print(inEntry)
        count2 = inEntry.pivot(index='dauid', columns='incident_cat', values='count_1')
        count2 = count2.fillna(0)
        weight2 = []
        fmax2 = []
        sweight = 0
        citer = 0
        for iter in args3.values():
            # if (float(iter) < 0):
            #     weight2.append(-1 * math.log(math.fabs(float(iter))))
            # elif (float(iter) >= 0 and float(iter) <= 1):
            #     weight2.append(float(iter))
            # else:
            #     weight2.append(math.log(math.fabs(float(iter))))
            weight2.append(float(iter))
            fmax2.append(count2.values[:, citer:citer + 1].max())
            sweight = sweight + math.fabs(float(iter))
            citer = citer + 1
        print(weight2)
        print(sweight)
        print(count2)
        fmax3 = np.multiply(fmax2, sweight)
        indicator = dict()
        for did, vl in zip(count2.index, count2.values):
            indicator[did] = (vl * weight2) / fmax3
        filler = np.zeros(len(weight2))
        for dess2 in dauID2.values:
            if dess2[0] not in indicator.keys():
                indicator[dess2[0]] = filler

        result2 = indicator.copy()
        for diss in indicator.keys():
            result2[diss] = sum(indicator[diss])
        sums = []
        result2max = max(result2.values())
        result2min = min(result2.values())
        resdiff = result2max - result2min
        resu3 = dict()
        indDic=dict()
        for key in result2.keys():
            resu3[key] = result2[key]
            indDic[str(key)] = resu3[key]

        for dauid in resu3.keys():
            sums.append(dict(DAUID=str(dauid), Index=resu3[dauid], X=valX, Y=indicator[dauid].tolist()))
        indVar['index'] = indDic
        resu4 = dict()
        for key2 in resu3.keys():
            resu4[str(key2)] = resu3[key2]
        ind = {}
        for did in indicator.keys():
            temp = str(did)
            ind[temp] = indicator[did].tolist()

        pl = Indices.get_next()
        indices = pd.read_sql(pl.statement, db.engine)
        indices = indices.fillna(0)
        last_ind = indices.values.flatten()[0]
        last_ind = int(last_ind) + 1
        iname = 'index' + str(last_ind)

        for ind in indVar['index']:
            ind2 = int(ind)
            d = Indices(index_name=iname, index_no=last_ind, dauid=ind2, index=indVar['index'][ind])
            db.session.add(d)
        db.session.commit()
        for propor in indVar['propor']:
            d2 = Indicesprop(index_name=iname, index_no=last_ind, variable=propor['ind'], weight=propor['weight'],
                             sign=propor['sign'], maxweight=propor['maxweight'], minweight=propor['minweight'])
            db.session.add(d2)
        db.session.commit()
        db.session.close()
        return dict(sums=sums, resu2=resu4, indic=ind, valX=valX)



class Collector(Resource):
    def get(self):
        ps = Indices.query.distinct(Indices.index_name.label('index_name'),
                                    Indices.index_no.label('index_no')).order_by(Indices.index_no)
        ind = pd.read_sql(ps.statement, db.engine)
        indicesArr=ind['index_name'].values
        indices=dict()
        indices['index1']=indicesArr.tolist()
        indices['index2']=indicesArr.tolist()
        return dict(indices=indices)

class Pie(Resource):
    def get(self):
        args=request.args
        print(args['value'])
        pl3 = Indicesprop.query.filter_by(index_name=args['value'].lower())
        indices3 = pd.read_sql(pl3.statement, db.engine)
        pie=dict()
        pie['variable']=indices3['variable'].values.tolist()
        pie['weight']=indices3['weight'].values.tolist()
        return dict(pie=pie)

# class Generator(Resource):
#     def get(self):
#         args=request.args
#         pl = Indices.get_matrix()
#         indices = pd.read_sql(pl.statement, db.engine)
#         indices1 = indices[indices.index_name.isin(args['index'])]
#         indices2 = indices1.pivot(index='dauid', columns='index_name', values='index')
#         ind_dict=indices2.values.tolist()
#         k = indices2.index.astype('str').to_list()
#         result=dict()
#         for iter in range(0,len(k)):
#             result[k[iter]]=ind_dict[iter]
#         return dict(result=result)

class Comparisor(Resource):
    def get(self):
        args=request.args
        iter=0
        v1 = []
        v2 = []
        for b in args:
            if(iter==0):
                v1.append(args[b].lower())
            else:
                v2.append(args[b].lower())
            iter=iter+1

        pl = Indices.get_matrix()
        indices = pd.read_sql(pl.statement, db.engine)
        indices1 = indices[indices.index_name.isin(v1)]
        indices2 = indices[indices.index_name.isin(v2)]
        indices1 = indices1.pivot(index='dauid', columns='index_name', values='index')
        indices2 = indices2.pivot(index='dauid', columns='index_name', values='index')
        ind1=indices1.values.flatten()
        ind2=indices2.values.flatten()
        cmp=(ind2-ind1)/2
        comp=cmp.tolist()
        k = indices2.index.astype('str').to_list()
        result = dict()
        for iter in range(0, len(k)):
            result[k[iter]] = comp[iter]
        return dict(result=result)




rest_api_bp = Blueprint('api', __name__)
api = Api(rest_api_bp)

api.add_resource(Index, '/')
api.add_resource(IndexCreation, '/index/')
api.add_resource(DemCreation,'/dindex/')
api.add_resource(Severity, '/severity/')
api.add_resource(Interpreter1,'/interpreter1/')
api.add_resource(Interpreter2,'/interpreter2/')
api.add_resource(Interpreter3,'/interpreter3/')
api.add_resource(Explainer,'/explainer/')
api.add_resource(IncCreation,'/incident/')
api.add_resource(Collector,'/collector/')
api.add_resource(Pie,'/pie/')
# api.add_reosurce(Generator,'/generate/')
api.add_resource(Comparisor,'/comparisor/')
api.add_resource(PcaCrime, '/crimepca/')