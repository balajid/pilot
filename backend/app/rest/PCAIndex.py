import json
import os

from flask import Blueprint, request
from flask_restful import Resource, Api
import pandas as pd
import index_creator
from app.model.police import Police
from app.model.demographic import Demographics
from app.model.incidents import Incidents
from app.model.indices import Indices
from app.model.indicesprop import Indicesprop
from app import db
import math
import numpy as np

def norm_0_1(x):
    x_norm = (x - x.min()) / (x.max() - x.min())
    return x_norm

def my_pca(x, kfactor=False):
    cov = np.dot(x.T, x)
    eigvals, eigvecs = np.linalg.eig(cov)
    alpha = eigvals / eigvals.sum()
    if kfactor:
        alpha = alpha[alpha > 0.01]
    k = alpha.shape[0]
    x_rotate = np.dot(x, eigvecs[:, :k])
    I = norm_0_1(np.dot(x_rotate, alpha))

    return I

class PcaCrime(Resource):
    def get(self):
        args = request.args
        valX = []
        wx = []
        sx = []
        maxx = []
        for a in args.keys():
            valX.append(a)
            max_index = Police.query.filter_by(ucr_description=a).first().severity_weight
            if max_index is None or 0: max_index = 1
            maxx.append(int(math.log(max_index)))
        for b in args.values():
            if (int(b) < 1):
                sx.append(-1)
                wx.append(math.fabs(int(b)))
            else:
                sx.append(1)
                wx.append(b)
        indVar = dict()
        vlen = len(valX)
        tempP = []
        for i in range(0, vlen):
            tempd = dict()
            tempd['ind'] = valX[i]
            tempd['weight'] = wx[i]
            tempd['maxweight'] = maxx[i]
            tempd['minweight'] = 0
            tempd['sign'] = sx[i]
            tempP.append(tempd)
        indVar['propor'] = tempP
        entries = Police.get_matrix()
        dentry = Demographics.get_dauID()
        dauID = pd.read_sql(dentry.statement, db.engine)

        df = pd.read_sql(entries.statement, db.engine)

        df = df[df.ucr_description.isin(args.keys())]
        count = df.pivot(index='dauid', columns='ucr_description', values='count_1')
        # sev = df.pivot(index='dauid', columns='ucr_description', values='severity_weight')
        count = count.fillna(0)
        # sev = sev.fillna(0)
        # args2 = ([float(a) for a in args.values()] * sev)
        weight = []
        fmax = []
        sumW = 0
        citer = 0
        for iter in args.values():
            # if (float(iter) < 0):
            #     weight.append(-1 * math.log(math.fabs(float(iter))))
            # elif (float(iter) >= 0 and float(iter) <= 1):
            #     weight.append(float(iter))
            # else:
            #     weight.append(math.log(math.fabs(float(iter))))
            weight.append(float(iter))
            fmax.append(count.values[:, citer:citer + 1].max())
            citer = citer + 1
            sumW = sumW + math.fabs(float(iter))
        count_x=count.values * wx
        count_pca=my_pca(count_x)
        indicator = dict()
        for did, vl in zip(count_pca.index, count_pca.values):
            indicator[did] = vl
        filler = np.zeros(len(weight))
        for dess in dauID.values:
            if dess[0] not in indicator.keys():
                indicator[dess[0]] = filler

        result2 = indicator.copy()
        for diss in indicator.keys():
            result2[diss] = sum(indicator[diss])
        sums = []
        result2max = max(result2.values())
        result2min = min(result2.values())
        resdiff = result2max - result2min
        indDic = dict()
        resu3 = dict()
        for key in result2.keys():
            resu3[key] = result2[key]
            indDic[str(key)] = resu3[key]
        for dauid in resu3.keys():
            sums.append(dict(DAUID=str(dauid), Index=resu3[dauid], X=valX, Y=indicator[dauid].tolist()))
        indVar['index'] = indDic

        resu4 = dict()
        for key2 in resu3.keys():
            resu4[str(key2)] = resu3[key2]
        ind = {}
        for did in indicator.keys():
            temp = str(did)
            ind[temp] = indicator[did].tolist()
        if (os.path.exists('index.json') == True):
            with open('index.json') as json_file:
                indexD = json.load(json_file)
                indexD['indices'].append(indVar)
        else:
            indexD = dict()
            indexD['indices'] = []
            indexD['indices'].append(indVar)
        pl = Indices.get_next()
        indices = pd.read_sql(pl.statement, db.engine)
        indices = indices.fillna(0)
        last_ind = indices.values.flatten()[0]
        last_ind = int(last_ind) + 1
        iname = 'index' + str(last_ind)
        for ind in indVar['index']:
            ind2 = int(ind)
            d = Indices(index_name=iname, index_no=last_ind, dauid=ind2, index=indVar['index'][ind])
            db.session.add(d)
        db.session.commit()
        for propor in indVar['propor']:
            d2 = Indicesprop(index_name=iname, index_no=last_ind, variable=propor['ind'], weight=propor['weight'],
                             sign=propor['sign'], maxweight=propor['maxweight'], minweight=propor['minweight'])
            db.session.add(d2)
        db.session.commit()
        db.session.close()
        print(len(indexD['indices']))
        # with open('index.json', 'w') as json_file:
        #     json.dump(indexD, json_file)
        return dict(sums=sums, resu2=resu4, indic=ind, valX=valX)


rest_api_bp = Blueprint('api', __name__)
api = Api(rest_api_bp)

api.add_resource(PcaCrime, '/crimepca/')