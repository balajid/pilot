import sys
import traceback

from flask import Blueprint, render_template, abort, redirect, session, url_for, make_response, jsonify, request
from jinja2 import TemplateNotFound
from app.model.police import Police

sample_page = Blueprint('sample_page', 'sample_page', template_folder='templates')


@sample_page.route('/')
def index():
    try:
        return render_template('index.html')
    except TemplateNotFound:
        abort(404)