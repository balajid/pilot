import os
from sshtunnel import SSHTunnelForwarder

class Config:
    SECRET_KEY = os.getenv('SECRET_KEY')
    CORS_ENABLED = True

    # if os.getenv('TUNNEL_IP') is None:
    #     SQLALCHEMY_DATABASE_URI = "postgresql://postgres:postgres@vav.research.cs.dal.ca/municipality"
    # else:
    SQLALCHEMY_DATABASE_URI = "postgresql://postgres:postgres@127.0.0.1:5432/municipality"
    SQLALCHEMY_TRACK_MODIFICATIONS = False

    # TEST_RESPONSES = False
    TEST_RESPONSES = True

    if os.getenv('SERVER_STATUS') == 'DEBUG':
        DEBUG = True
    elif os.getenv('SERVER_STATUS') == 'TEST':
        TESTING = True
        DEBUG = True
