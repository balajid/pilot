import os

from flask_sqlalchemy import SQLAlchemy
import click
from flask import Flask
from flask_cors import CORS
from flask_migrate import Migrate

from werkzeug.middleware.proxy_fix import ProxyFix

from app.config import Config

app = Flask(__name__,static_url_path='',
            static_folder='templates',
            template_folder='templates')
app.config.from_object(Config)
CORS(app, resources={r"/*": {"origins": "*"}})
db = SQLAlchemy(app)
migrate = Migrate(app, db)



from .views import sample_page
app.register_blueprint(sample_page)

from .rest import rest_api_bp
app.register_blueprint(rest_api_bp, url_prefix='/rest')

# from .views import auth
# app.register_blueprint(auth, url_prefix='/auth')
#
# from app.rest.webhooks import webhooks_bp
# app.register_blueprint(webhooks_bp, url_prefix='/webhooks')
