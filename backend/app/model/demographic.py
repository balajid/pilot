from sqlalchemy import func

from app import db


class Demographics(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    dauid = db.Column(db.BigInteger, index=True)
    total = db.Column(db.Integer)
    male = db.Column(db.Integer)
    female = db.Column(db.Integer)
    attribute = db.Column(db.String(400))

    def __repr__(self):
        return str(self.__dict__)

    @staticmethod
    def get_matrix():
        return Demographics.query.with_entities(Demographics.dauid,
                                                Demographics.attribute,
                                                Demographics.total)

    @staticmethod
    def get_dauID():
        return Demographics.query.with_entities(Demographics.dauid.distinct())
