from app import db
from sqlalchemy import func

class Incidents(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    fid_hrfe  = db.Column(db.BigInteger)
    incident_d = db.Column(db.BigInteger)
    incident_r = db.Column(db.String(100))
    incident_cat = db.Column(db.String(100))
    dauid = db.Column(db.BigInteger)
    ctuid = db.Column(db.Float)

    @staticmethod
    def get_matrix():
        return Incidents.query.with_entities(Incidents.dauid,
                                          Incidents.incident_cat,
                                          func.count(),
                                          ) \
            .group_by(Incidents.dauid,
                      Incidents.incident_cat)
