from app import db
from sqlalchemy import func

class Indices(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    index_name = db.Column(db.String(500))
    index_no = db.Column(db.BigInteger)
    dauid = db.Column(db.BigInteger)
    index = db.Column(db.Float)

    @staticmethod
    def get_matrix():
        return Indices.query.with_entities(Indices.index_name,
                                           Indices.index_no,
                                          Indices.dauid,
                                          Indices.index
                                          )

    @staticmethod
    def get_next():
        return Indices.query.with_entities(func.max(Indices.index_no))