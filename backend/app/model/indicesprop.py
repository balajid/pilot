from app import db
from sqlalchemy import func

class Indicesprop(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    index_name = db.Column(db.String(100))
    index_no = db.Column(db.BigInteger)
    variable = db.Column(db.String(100))
    weight = db.Column(db.Integer)
    sign = db.Column(db.Integer)
    maxweight = db.Column(db.Integer)
    minweight = db.Column(db.Integer)

    @staticmethod
    def get_matrix():
        return Indicesprop.query.with_entities(Indicesprop.index_name,
                                          Indicesprop.index_no,
                                          Indicesprop.variable,
                                          Indicesprop.weight,
                                          Indicesprop.sign,
                                          Indicesprop.maxweight,
                                          Indicesprop.minweight
                                          ) \
            .group_by(Indicesprop.index_no)

