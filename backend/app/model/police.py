from sqlalchemy import func,distinct
from app import db


class Police(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    incident_start_time = db.Column(db.Integer)
    ucr = db.Column(db.Integer)
    severity_weight = db.Column(db.Float)
    ucr_description = db.Column(db.String(100))
    ext = db.Column(db.Integer)
    dauid = db.Column(db.Integer)
    category = db.Column(db.String(100))
    subcategory = db.Column(db.String(100))
    incident_start_date = db.Column(db.Date)

    @staticmethod
    def get_matrix():
        return Police.query.with_entities(Police.dauid,
                                          # Police.category,
                                          # Police.subcategory,
                                          Police.ucr_description,
                                          func.count(),
                                          func.avg(Police.severity_weight).label('severity_weight')
                                          )\
            .group_by(Police.dauid,
                      # Police.category,
                      # Police.subcategory,
                      Police.ucr_description)

    @staticmethod
    def get_dauID():
        return Police.query.with_entities(Police.dauid.distinct())

    @staticmethod
    def get_sortedmatrix(d1,d2):
        return Police.query.with_entities(Police.dauid,
                                          Police.ucr_description,
                                          func.count(),
                                          func.avg(Police.severity_weight).label('severity_weight')
                                          )\
                    .filter(Police.incident_start_date.between(d1, d2))\
                    .group_by(Police.dauid,
                              # Police.category,
                              # Police.subcategory,
                              Police.ucr_description)

