import requests
import pandas as pd
import io
import json
import pprint

with open('../app/data/Dissemination6.json') as JFile:
    jsonData = json.load(JFile)
    featureData = jsonData['features']
    dataSize=601
    count=0
    percent=10
    for iterData in featureData:
        
        dauid = iterData['properties']['DAUID']
        count=count+1
        print(dauid,count)
        if count>= (dataSize*(percent/100)):
            print((percent),"% completed")
            percent=percent+10
        
        demographics_url = f'https://www12.statcan.gc.ca/census-recensement/2016/dp-pd/prof/details/download-telecharger/' \
                           f'current-actuelle.cfm?Lang=E&Geo1=DA&Code1={dauid}&Geo2=PR&Code2=01&B1=All&type=0&FILETYPE=CSV'
        
        csvfile = str(requests.get(demographics_url).content)
        csvfile = csvfile.replace('\\r\\n', '\n')
        csvfile = csvfile[csvfile.index('\n'):csvfile.index('Symbols:')]
        csvfile = io.StringIO(csvfile)
        df = pd.read_csv(csvfile)
        
        df = df[pd.isna(df['Topic']) == False]
        df = df[df['Topic'] != 'Population and dwellings']
        df['Total'] = df['Total'].fillna(-1)
        df['Male'] = df['Male'].fillna(-1)
        df['Female'] = df['Female'].fillna(-1)
        
        ret2 = ['']
        hir = []
        hir_i = 0
        head='';
        flag=0
        for index, row in df.iterrows():
            if(head!=row['Topic']):
                hir=[]
                head = row['Topic']
                hir.append(head)
                hir_i=0
                flag=0
            hir_n = (len(row['Characteristics']) - len(row['Characteristics'].lstrip(' ')))/2
            if(hir_n==0 and flag==0):
                flag=1
                continue
            if hir_n > hir_i:
                hir.append(row['Characteristics'].lstrip(' '))
                hir_i += 1
                ret2.pop()
            elif hir_n < hir_i:
                while(hir_n<=hir_i):
                    if(hir[-1]==row['Topic']):
                        hir_i -= 1
                        break
                    hir.pop()
                    hir_i -= 1
                hir.append(row['Characteristics'].lstrip(' '))
                hir_i +=1
            else:
                hir.pop()
                hir.append(row['Characteristics'].lstrip(' '))
            ret2.append(['|'.join(hir).replace('Total - ', ''), float(row['Total']), float((str(row['Male']).replace(" ",""))), float(row['Female'])])
        # pp=pprint.PrettyPrinter()
        # pp.pprint(ret2)
        # print(len(ret2))

        from app.model.demographic import Demographics, db

        for i in ret2:
            d = Demographics(dauid=dauid, attribute=i[0], total=int(i[1]), male=int(i[2]), female=int(i[3]))
            db.session.add(d)
        db.session.commit()
    db.session.close()

