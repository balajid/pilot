import json

with open('index.json') as json_file:
    indexD = json.load(json_file)
    ct=0
    from app.model.indices import Indices, db
    for index in indexD['indices']:
        ct=ct+1
        iname='index'+str(ct)
        for ind in index['index']:
            ind2=int(ind)
            d=Indices(index_name=iname,dauid=ind2,index=index['index'][ind])
            db.session.add(d)

    db.session.commit()
    db.session.close()