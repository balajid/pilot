# Backend code

First set up your environment:

```bash
cd <project path>/backend
virtualenv -p python3 ./venv
source ./venv/bin/activate
pip install -r ./requirements.txt
```

To start the server, run:
```bash
export FLASK_APP=wsgi.py
export SERVER_STATUS=DEBUG
python ./wsgi.py
```

If you want your server to be visible from the outside (other than your computer):
```bash
export FLASK_APP=wsgi.py
export SERVER_STATUS=DEBUG
flask run --host 0.0.0.0
```