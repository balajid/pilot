# %%
from lime import lime_tabular

import mlconnector as ml
import numpy as np
import pandas as pd
from tpot import TPOTRegressor
import timeit


# %%

### Compute mean and standard deviation
def mean_sd(x):
    mean = pd.DataFrame(x.mean()).T
    sd = pd.DataFrame(x.std()).T
    res = pd.concat([mean, sd], axis=0)
    res.index = ["Mean", "STD"]

    return res

def norm_0_1(x):
    x_norm = (x - x.min()) / (x.max() - x.min())
    return x_norm

# computing R-squared and adjusted R squared
def R_score(y, y_p, k):
    y_mean = float(y.mean())
    sq_tot = ((y - y_mean) ** 2).sum()
    sq_res = ((y - y_p) ** 2).sum()
    sq_exp = ((y_p - y_mean) ** 2).sum()

    R2 = sq_exp / sq_tot
    R2_norm = sq_exp / (sq_exp + sq_res)
    R2_2 = 1 - (sq_res / sq_tot)
    adjusted_R = 1 - ((y.shape[0] - 1) / (y.shape[0] - (k + 1))) * (1 - R2)
    Standard_e = np.sqrt(((1 / (len(y) - k - 1)) * sq_res))
    MSE = sq_res / len(y)
    RMSE = np.sqrt(MSE)

    all_r = pd.DataFrame({'sq_tot': [sq_tot], 'sq_res': [sq_res], 'sq_exp': [sq_exp],
                          'R-squared': [R2], 'R-squared_norm': [R2_norm], 'R-squared_2': [R2_2], 'adjustedR': [adjusted_R],
                          'Standard_e': [Standard_e], 'MSE': [MSE], 'RMSE': [RMSE]},
                         columns=['sq_tot', 'sq_res', 'sq_exp',
                                  'R-squared', 'R-squared_norm', 'R-squared_2', 'adjustedR', 'Standard_e', 'MSE', 'RMSE'])

    return all_r


###############
# AUTOML TPOT
###############
def tpot_automl(x, y, ps=100, gn=100):
    # Running automl regression
    # instantiate tpot
    start_time = timeit.default_timer()
    tpot = TPOTRegressor(verbosity=2, population_size=ps,
                         generations=gn, n_jobs=8)
    tpot.fit(x, y.to_numpy().ravel())
    tpot_score = tpot.score(x, y.to_numpy().ravel())
    elapsed = timeit.default_timer() - start_time
    best_pipeline = tpot.fitted_pipeline_

    # Compute R-squared
    my_score = R_score(y.to_numpy().ravel(), tpot.predict(x).ravel(), x.shape[1])
    my_score.index = ['tpot']

    print('Scores:', tpot_score)
    print('Scores:', my_score)
    print(best_pipeline)
    print('Times:', elapsed)

    return my_score, elapsed, best_pipeline, tpot

# %%
################
# CREATING MODEL
################
# def apply_regression(
number_of_variables_to_index = 5
number_of_variables_to_correlate = 8

### POLICE DATA
# reading HRPC
mp = ml.get_matrix()
subcategories = np.unique(mp.ucr)
all_data_p_names = pd.crosstab(index=mp.ucr, columns=mp.ucr_description, values=mp.count_1, aggfunc='sum').fillna(0)
all_data_p = pd.crosstab(index=mp.dauid, columns=mp.ucr, values=mp.count_1, aggfunc='sum').fillna(0)

# select the range
numbers_cat = all_data_p.columns[all_data_p.columns < 4000]
numbers_cat2 = all_data_p.columns[all_data_p.columns >= 4000]

# get the severity index
mp['severity_weight'].fillna(np.e, inplace=True)
severity_values = pd.pivot_table(mp, values='severity_weight', index="ucr", aggfunc=np.unique)
severity_weights = np.log(severity_values)

# datetime object containing current date and time to be used as seed
now = timeit.default_timer()
rng = np.random.RandomState(int(now))

# Selecting categories randonly
# idxCat = np.sort(rng.choice(all_data_p.shape[1], number_of_variables_to_index, replace=False), axis=0)
nv = int(np.floor(number_of_variables_to_index * .6))
idxCat = np.sort(rng.choice(numbers_cat, nv, replace=False), axis=0)
idxCat = np.hstack(
    [idxCat, np.sort(rng.choice(numbers_cat2, (number_of_variables_to_index - nv), replace=False), axis=0)])
# idxCat = [3365.0, 4120.0, 4330.0, 1410.0, 4240.0, 6900.0] # correlated
# idxCat = [1510.0, 4330.0, 6510.0, 3560.0, 9450.0, 9510.0] # not correlated
# idxCat = [1371.0, 3115.0, 3740.0, 4110.0, 9520.0] # not correlated
# idxCat = [2153.0, 3410.0, 3490.0, 4130.0, 4220.0] # correlated

# Creating Index
used_columns_for_index_creation = all_data_p_names.iloc[:, [all_data_p.columns.get_loc(c) for c in idxCat if c in all_data_p]].columns
used_weights_for_index_creation = severity_weights.loc[idxCat]
X = all_data_p.loc[:, idxCat].to_numpy()
w = severity_weights.loc[idxCat].to_numpy()
X_w = X * w.T
labels = norm_0_1(X_w.sum(axis=1))
labels = pd.DataFrame(labels)
labels = labels.rename(columns={0: 'output'})
labels.index = all_data_p.index

# %%
### DEMOGRAPHIC DATA
m = ml.get_demographic_matrix()
all_data = pd.crosstab(index=m.dauid, columns=m.attribute, values=m.total, aggfunc='sum').fillna(0)
# removing missing values
all_data.replace(to_replace=-1, value=np.nan, inplace=True)
all_data = all_data.dropna()

# selecting random demographics variables
# idxCorr = np.sort(rng.choice(all_data.shape[1], number_of_variables_to_correlate, replace=False), axis=0)
# idxCorr = [20, 21, 22, 32, 835, 846, 943, 953] #selected by hand
id_nvc = int(number_of_variables_to_correlate/8)
idxCorr = np.sort(rng.choice(20, id_nvc, replace=False), axis=0)
idxCorr = np.hstack([idxCorr, 21])
idxCorr = np.hstack([idxCorr, np.sort(rng.choice(range(22, 32), id_nvc,  replace=False), axis=0)])
idxCorr = np.hstack([idxCorr, np.sort(rng.choice(range(32, 835), id_nvc,  replace=False), axis=0)])
idxCorr = np.hstack([idxCorr, np.sort(rng.choice(range(835, 846), id_nvc,  replace=False), axis=0)])
idxCorr = np.hstack([idxCorr, np.sort(rng.choice(range(846, 943), id_nvc,  replace=False), axis=0)])
idxCorr = np.hstack([idxCorr, np.sort(rng.choice(range(943, 953), id_nvc,  replace=False), axis=0)])
idxCorr = np.hstack([idxCorr, np.sort(rng.choice(range(953, all_data.shape[1]), id_nvc,  replace=False), axis=0)])
input_data = all_data.iloc[:, idxCorr]

### Adjustment of data
# working with the same DAUID
DAUID = set(input_data.index.unique())
DAUID_p = set(labels.index.unique())
idx_DAUID = DAUID.intersection(DAUID_p)

input_data = input_data.loc[list(idx_DAUID), :]
labels = labels.loc[list(idx_DAUID)]

# %%
### REGRESSION ANALYSIS
number_population = 20
number_generation = 20

print("TPOT - AUTOML\n")
tpot_score, tpot_elapsed, tpot_best_pipeline, tpot_model = tpot_automl(input_data, labels, ps=number_population, gn=number_generation)
tpot_score_final = tpot_score[['R-squared_2', 'Standard_e', 'MSE', 'RMSE']]

### iNTERPRETATION
exp = lime_tabular.LimeTabularExplainer(input_data.as_matrix(),
                                        mode='regression',
                                        kernel_width=3,
                                        verbose=True,
                                        feature_names=input_data.columns)
input_data_id = np.random.randint(input_data.shape[0])
input_data_sample = input_data.iloc[input_data_id, :]
input_data_label = labels.iloc[input_data_id]
explanation = exp.explain_instance(input_data_sample, tpot_model.predict,
                                   num_features=number_of_variables_to_correlate,
                                   top_labels=1)
explanation.as_list()

input_data_id = labels[labels.to_numpy() == 1].index
input_data_id = labels.index.get_loc(input_data_id.array[0])
input_data_sample = input_data.iloc[input_data_id, :]
input_data_label = labels.iloc[input_data_id]
explanation = exp.explain_instance(input_data_sample, tpot_model.predict,
                                   num_features=number_of_variables_to_correlate,
                                   top_labels=1)
explanation.as_list()