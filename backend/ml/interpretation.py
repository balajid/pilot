from lime import lime_tabular

import mlconnector as ml
import numpy as np
import pandas as pd
from tpot import TPOTRegressor
import timeit


### Compute mean and standard deviation
def mean_sd(x):
    mean = pd.DataFrame(x.mean()).T
    sd = pd.DataFrame(x.std()).T
    res = pd.concat([mean, sd], axis=0)
    res.index = ["Mean", "STD"]

    return res


def norm_0_1(x):
    x_norm = (x - x.min()) / (x.max() - x.min())
    return x_norm


# computing R-squared and adjusted R squared
def R_score(y, y_p, k):
    y_mean = float(y.mean())
    sq_tot = ((y - y_mean) ** 2).sum()
    sq_res = ((y - y_p) ** 2).sum()
    sq_exp = ((y_p - y_mean) ** 2).sum()

    R2 = sq_exp / sq_tot
    R2_norm = sq_exp / (sq_exp + sq_res)
    R2_2 = 1 - (sq_res / sq_tot)
    adjusted_R = 1 - ((y.shape[0] - 1) / (y.shape[0] - (k + 1))) * (1 - R2)
    Standard_e = np.sqrt(((1 / (len(y) - k - 1)) * sq_res))
    MSE = sq_res / len(y)
    RMSE = np.sqrt(MSE)

    all_r = pd.DataFrame({'sq_tot': [sq_tot], 'sq_res': [sq_res], 'sq_exp': [sq_exp],
                          'R-squared': [R2], 'R-squared_norm': [R2_norm], 'R-squared_2': [R2_2],
                          'adjustedR': [adjusted_R],
                          'Standard_e': [Standard_e], 'MSE': [MSE], 'RMSE': [RMSE]},
                         columns=['sq_tot', 'sq_res', 'sq_exp',
                                  'R-squared', 'R-squared_norm', 'R-squared_2', 'adjustedR', 'Standard_e', 'MSE',
                                  'RMSE'])

    return all_r


###############
# AUTOML TPOT
###############
def tpot_automl(x, y, ps=100, gn=100):
    # Running automl regression
    # instantiate tpot
    start_time = timeit.default_timer()
    tpot = TPOTRegressor(verbosity=2, population_size=ps,
                         generations=gn, n_jobs=8)
    tpot.fit(x, y.to_numpy().ravel())
    tpot_score = tpot.score(x, y.to_numpy().ravel())
    elapsed = timeit.default_timer() - start_time
    best_pipeline = tpot.fitted_pipeline_

    # Compute R-squared
    my_score = R_score(y.to_numpy().ravel(), tpot.predict(x).ravel(), x.shape[1])
    my_score.index = ['tpot']

    print('Scores:', tpot_score)
    print('Scores:', my_score)
    print(best_pipeline)
    print('Times:', elapsed)

    return my_score, elapsed, best_pipeline, tpot

def regression3(indexes, variables, number_population=50, number_generation=50):
    """

    :param indexes: index produced with police data. It should be a pandas Dataframe.
    :param variables: demographic variables under analysis. It should be the column names.
    :param number_population: genetic algorithm information.
    :param number_generation: genetic algorithm information.
    :return:
    tpot_model: entire model.
    input_data: data provided by regression function.
    indexes: original index produced with police data.
    predict_indexes: index predicted by the model.
    tpot_score_final: coefficient of determination and errors measurements.
    """
    ### DEMOGRAPHIC DATA
    #m = ml.get_demographic_matrix()
    m=ml.get_matrix()
    #all_data = pd.crosstab(index=m.dauid, columns=m.attribute, values=m.total, aggfunc='sum').fillna(0)
    all_data = pd.crosstab(index=m.dauid, columns=m.ucr_description,values=m.count_1, aggfunc='sum').fillna(0)
    # removing missing values
    all_data.replace(to_replace=-1, value=0, inplace=True)
    all_data.replace(to_replace=np.nan, value=0, inplace=True)
    # all_data = all_data.dropna()
    # selecting demographics variables
    input_data = all_data.loc[:, variables]

    ### Adjustment of data
    input_data.index = input_data.index.astype('int64')
    indexes.index = indexes.index.astype('int64')
    # working with the same DAUID
    DAUID = set(input_data.index.unique())
    DAUID_p = set(indexes.index.unique())
    idx_DAUID = DAUID.intersection(DAUID_p)

    input_data2 = input_data.loc[list(idx_DAUID), :]
    indexes = indexes.loc[list(idx_DAUID)]

    ### REGRESSION ANALYSIS
    tpot_score, tpot_elapsed, tpot_best_pipeline, tpot_model = tpot_automl(input_data2, indexes, ps=number_population,
                                                                           gn=number_generation)
    tpot_score_final = tpot_score[['R-squared_2', 'Standard_e', 'MSE', 'RMSE']]
    predict_indexes = tpot_model.predict(input_data)

    return tpot_model, input_data, indexes, predict_indexes, tpot_score_final


################
# INTERPRETATION
################
def regression2(indexes, variables, number_population=50, number_generation=50):
    """

    :param indexes: index produced with police data. It should be a pandas Dataframe.
    :param variables: demographic variables under analysis. It should be the column names.
    :param number_population: genetic algorithm information.
    :param number_generation: genetic algorithm information.
    :return:
    tpot_model: entire model.
    input_data: data provided by regression function.
    indexes: original index produced with police data.
    predict_indexes: index predicted by the model.
    tpot_score_final: coefficient of determination and errors measurements.
    """
    ### DEMOGRAPHIC DATA
    #m = ml.get_demographic_matrix()
    m=ml.get_incident_matrix()
    #all_data = pd.crosstab(index=m.dauid, columns=m.attribute, values=m.total, aggfunc='sum').fillna(0)
    all_data = pd.crosstab(index=m.dauid, columns=m.incident_cat,values=m.count_1, aggfunc='sum').fillna(0)
    # removing missing values
    all_data.replace(to_replace=-1, value=0, inplace=True)
    all_data.replace(to_replace=np.nan, value=0, inplace=True)
    # all_data = all_data.dropna()
    # selecting demographics variables
    input_data = all_data.loc[:, variables]

    ### Adjustment of data
    input_data.index = input_data.index.astype('int64')
    indexes.index = indexes.index.astype('int64')
    # working with the same DAUID
    DAUID = set(input_data.index.unique())
    DAUID_p = set(indexes.index.unique())
    idx_DAUID = DAUID.intersection(DAUID_p)

    input_data2 = input_data.loc[list(idx_DAUID), :]
    indexes = indexes.loc[list(idx_DAUID)]

    ### REGRESSION ANALYSIS
    tpot_score, tpot_elapsed, tpot_best_pipeline, tpot_model = tpot_automl(input_data2, indexes, ps=number_population,
                                                                           gn=number_generation)
    tpot_score_final = tpot_score[['R-squared_2', 'Standard_e', 'MSE', 'RMSE']]
    predict_indexes = tpot_model.predict(input_data)

    return tpot_model, input_data, indexes, predict_indexes, tpot_score_final

def regression1(indexes, variables, number_population=50, number_generation=50):
    """

    :param indexes: index produced with police data. It should be a pandas Dataframe.
    :param variables: demographic variables under analysis. It should be the column names.
    :param number_population: genetic algorithm information.
    :param number_generation: genetic algorithm information.
    :return:
    tpot_model: entire model.
    input_data: data provided by regression function.
    indexes: original index produced with police data.
    predict_indexes: index predicted by the model.
    tpot_score_final: coefficient of determination and errors measurements.
    """
    ### DEMOGRAPHIC DATA
    m = ml.get_demographic_matrix()
    #m=ml.get_incident_matrix()
    all_data = pd.crosstab(index=m.dauid, columns=m.attribute, values=m.total, aggfunc='sum').fillna(0)
    #all_data = pd.crosstab(index=m.dauid, columns=m.incident_cat,values=m.count_1, aggfunc='sum').fillna(0)
    # removing missing values
    all_data.replace(to_replace=-1, value=0, inplace=True)
    all_data.replace(to_replace=np.nan, value=0, inplace=True)
    # all_data = all_data.dropna()
    # selecting demographics variables
    input_data = all_data.loc[:, variables]

    ### Adjustment of data
    input_data.index = input_data.index.astype('int64')
    indexes.index = indexes.index.astype('int64')
    # working with the same DAUID
    DAUID = set(input_data.index.unique())
    DAUID_p = set(indexes.index.unique())
    idx_DAUID = DAUID.intersection(DAUID_p)

    input_data2 = input_data.loc[list(idx_DAUID), :]
    indexes = indexes.loc[list(idx_DAUID)]

    ### REGRESSION ANALYSIS
    tpot_score, tpot_elapsed, tpot_best_pipeline, tpot_model = tpot_automl(input_data2, indexes, ps=number_population,
                                                                           gn=number_generation)
    tpot_score_final = tpot_score[['R-squared_2', 'Standard_e', 'MSE', 'RMSE']]
    predict_indexes = tpot_model.predict(input_data)

    return tpot_model, input_data, indexes, predict_indexes, tpot_score_final


def interpretation(dauid, indexes, predict_indexes, model, input_data, number_of_variables=0):
    """

    :param dauid: number of one dissemination area (int).
    :param indexes: original index produced with police data. It should be a pandas Dataframe.
    :param predict_indexes: index predicted by the model.
    :param model: model provided by regression function.
    :param input_data: data provided by regression function.
    :param number_of_variables: top variables that should be return for analyzes. If zero, it will return all.
    :return:
    explanation: impact of each variable in the DAUID index (as_list()).
    input_data_index: original indexes.
    predict_index: predict indexes.
    """
    if number_of_variables == 0:
        number_of_variables = input_data.shape[1]

    ### iNTERPRETATION
    exp = lime_tabular.LimeTabularExplainer(input_data.as_matrix(),
                                            mode='regression',
                                            kernel_width=3,
                                            verbose=True,
                                            feature_names=input_data.columns)

    indexes.index = indexes.index.astype('int64')
    input_data_id = indexes.index.get_loc(int(dauid))
    input_data_sample = input_data.iloc[input_data_id, :]
    explanation = exp.explain_instance(input_data_sample, model.predict,
                                       num_features=number_of_variables,
                                       top_labels=1)

    input_data_index = indexes.iloc[input_data_id]
    predict_index = predict_indexes[input_data_id]

    return explanation.as_list(), input_data_index, predict_index
