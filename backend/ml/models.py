import mlconnector as ml
import regression as rg
import numpy as np
import pandas as pd
import h2o
from h2o.estimators.glm import H2OGeneralizedLinearEstimator
from tpot import TPOTRegressor
import timeit


###############
# PREPARING OBJECT WITH MODELS
###############
# Contains models and results
class Models:
    # Contain all models produced
    def __init__(self, x, y):
        self.linear_regression = rg.sk_linear_regression_model(x, y)
        # self.polynomial_regression = rg.sk_linear_regression_model(x, labels, polynomial_degree=2)
        self.decision_tree = rg.regression_tree_model(x, y)
        self.random_forest = rg.random_tree_regression(x, y)
        self.xgbr = rg.xgbr(x, y)
        self.logistic_regression = rg.logistic_regression_model(x, y)
        self.ard = rg.ard_regression(x, y)
        self.lars = rg.lars(x, y)
        self.lasso_lars = rg.lasso_lars_regression(x, y)
        # self.sgd = rg.sgd_regression(x, y)
        self.bayesian_ridge = rg.bayesian_ridge_regression(x, y)
        self.passive_aggressive = rg.passive_aggressive_regression(x, y)
        self.theil_sen = rg.theil_sen_regression(x, y)
        # self.ransac = rg.ransac(x, y)
        self.huber = rg.huber(x, y)
        self.omp = rg.omp(x, y)
        # Scores
        self.scores = pd.DataFrame()
        my_score = rg.R_score(y, rg.prediction(self.linear_regression, x), x.shape[1])
        self.scores = pd.concat([self.scores, my_score], ignore_index=True)
        my_score = rg.R_score(y, rg.prediction(self.decision_tree, x), x.shape[1])
        self.scores = pd.concat([self.scores, my_score], ignore_index=True)
        my_score = rg.R_score(y, rg.prediction(self.random_forest, x), x.shape[1])
        self.scores = pd.concat([self.scores, my_score], ignore_index=True)
        my_score = rg.R_score(y, rg.prediction(self.xgbr, x), x.shape[1])
        self.scores = pd.concat([self.scores, my_score], ignore_index=True)
        my_score = rg.R_score(y, rg.prediction(self.logistic_regression, x), x.shape[1])
        self.scores = pd.concat([self.scores, my_score], ignore_index=True)
        my_score = rg.R_score(y, rg.prediction(self.ard, x), x.shape[1])
        self.scores = pd.concat([self.scores, my_score], ignore_index=True)
        my_score = rg.R_score(y, rg.prediction(self.lars, x), x.shape[1])
        self.scores = pd.concat([self.scores, my_score], ignore_index=True)
        my_score = rg.R_score(y, rg.prediction(self.lasso_lars, x), x.shape[1])
        self.scores = pd.concat([self.scores, my_score], ignore_index=True)
        # my_score = rg.R_score(y, rg.prediction(self.sgd, x), x.shape[1])
        # self.scores = pd.concat([self.scores, my_score], ignore_index=True)
        my_score = rg.R_score(y, rg.prediction(self.bayesian_ridge, x), x.shape[1])
        self.scores = pd.concat([self.scores, my_score], ignore_index=True)
        my_score = rg.R_score(y, rg.prediction(self.passive_aggressive, x), x.shape[1])
        self.scores = pd.concat([self.scores, my_score], ignore_index=True)
        my_score = rg.R_score(y, rg.prediction(self.theil_sen, x), x.shape[1])
        self.scores = pd.concat([self.scores, my_score], ignore_index=True)
        # my_score = rg.R_score(y, rg.prediction(self.ransac, x), x.shape[1])
        # self.scores = pd.concat([self.scores, my_score], ignore_index=True)
        my_score = rg.R_score(y, rg.prediction(self.huber, x), x.shape[1])
        self.scores = pd.concat([self.scores, my_score], ignore_index=True)
        my_score = rg.R_score(y, rg.prediction(self.omp, x), x.shape[1])
        self.scores = pd.concat([self.scores, my_score], ignore_index=True)
        self.scores.index = ['linear_regression', 'decision_tree',
                             'random_forest', 'xgbr', 'logistic_regression',
                             'ard', 'lars', 'lasso_lars', 'bayesian_ridge',
                             'passive_aggressive', 'theil_sen', 'huber', 'omp']


### Compute Pearson correlation
def idx_cov(x, y):
    covx = []
    for i in range(x.shape[1]):
        # covx.append(np.cov(x[:, i], y)[0,1])
        covx.append((sum((x[:, i] - x[:, i].mean()) * (y - y.mean()))) / ((x.shape[0] - 1) * x[:, i].std() * y.std()))
    idx = (-np.array(covx)).argsort()

    return idx


### Compute mean and standrd deviation
def mean_sd(x):
    mean = pd.DataFrame(x.mean()).T
    sd = pd.DataFrame(x.std()).T
    res = pd.concat([mean, sd], axis=0)

    return res


###############
# MY AUTOML
###############
def my_automl(x, y):
    # Centralizing data
    # scaler = StandardScaler()
    # all_data = scaler.transform(all_data)
    x2 = x - x.mean()

    y2 = y.to_numpy().ravel()
    # run model with all
    # test data
    start_time = timeit.default_timer()
    mymodels = Models(x2, y2)
    elapsed = timeit.default_timer() - start_time
    print('Times:', elapsed)
    print(mymodels.scores)

    return mymodels.scores


###############
# AUTOML TPOT
###############
def tpot_automl(x, y, ps=100, gn=100):
    # Running automl regression
    # instantiate tpot
    start_time = timeit.default_timer()
    tpot = TPOTRegressor(verbosity=2, population_size=ps,
                         generations=gn, n_jobs=4)
    tpot.fit(x, y.to_numpy().ravel())
    tpot_score = tpot.score(x, y.to_numpy().ravel())
    elapsed = timeit.default_timer() - start_time
    best_pipeline = tpot.fitted_pipeline_

    # Compute R-squared
    my_score = rg.R_score(y.to_numpy().ravel(), tpot.predict(x).ravel(), x.shape[1])
    my_score.index = ['tpot']

    print('Scores:', tpot_score)
    print('Scores:', my_score)
    print(best_pipeline)
    print('Times:', elapsed)

    return my_score, elapsed, best_pipeline, tpot


#################################
# CLASSIFICATION WITH H2O AUTOML regression GLM
#################################
def h2o_glm(x, y):
    h2o.init()

    # concat input data and labels
    ylabels = y.columns.to_list()[0]
    x = pd.concat([x, y], axis=1)
    xlabels = x.columns.to_list()
    train = h2o.H2OFrame(x)

    family_sets = ['gaussian',
                   # 'binomial','multinomial','ordinal', # y should be categorical
                   # 'quasibinomial',
                   'poisson',
                   'negativebinomial',
                   # 'gamma',
                   'tweedie']

    results = pd.DataFrame()

    for i in family_sets:
        print(i)
        # Set up GLM for regression
        start_time = timeit.default_timer()
        glm = H2OGeneralizedLinearEstimator(family=i, model_id='glm_default')
        # Use .train() to build the model
        glm.train(x=xlabels,
                  y=ylabels,
                  training_frame=train)

        predictions = glm.predict(train)
        y_p = predictions.as_data_frame()
        # Compute R-squared
        my_score = rg.R_score(y.to_numpy().ravel(), y_p.to_numpy().ravel(), x.shape[1])
        results = pd.concat([results, my_score], ignore_index=True)

    results.index = family_sets
    elapsed = timeit.default_timer() - start_time
    print('Times:', elapsed)
    print(results)

    h2o.shutdown()

    return results


################
# STATISTICAL VALIDATION
################
def analyses_models(number_of_variables_to_index=5, number_of_variables_to_correlate=5, iter=30):
    # POLICE DATA
    # getting police data frequency and severities
    mp = ml.get_matrix()
    all_data_p = pd.crosstab(index=mp.dauid, columns=mp.ucr_description, values=mp.count_1, aggfunc='sum').fillna(0)
    w_p = pd.crosstab(index=mp.dauid, columns=mp.ucr_description, values=mp.severity_weight, aggfunc='sum').fillna(0)
    # datetime object containing current date and time to be used as seed
    now = timeit.default_timer()
    rng = np.random.RandomState(int(now))

    # DEMOGRAPHIC DATA
    m = ml.get_demographic_matrix()
    all_data = pd.crosstab(index=m.dauid, columns=m.attribute, values=m.total, aggfunc='sum').fillna(0)
    # removing missing values
    all_data.replace(to_replace=-1, value=np.nan, inplace=True)
    all_data = all_data.dropna()

    all_scores = pd.DataFrame()
    all_time = pd.DataFrame()

    for i in range(iter):
        # selecting random variables
        idxCat = np.sort(rng.choice(all_data_p.shape[1], number_of_variables_to_index, replace=False), axis=0)
        # creating index
        labels = np.sum((all_data_p.iloc[:, idxCat] * w_p.iloc[:, idxCat]), axis=1)
        labels = (labels / labels.max())  # normalizing
        labels = pd.DataFrame(labels.to_frame())
        labels = labels.rename(columns={0: 'output'})
        # selecting random variables
        idxCorr = np.sort(rng.choice(all_data.shape[1], number_of_variables_to_correlate, replace=False), axis=0)
        # idxCorr = idx_cov(all_data.to_numpy(), labels.to_numpy())[0:number_of_variables_to_correlate]
        input_data = all_data.iloc[:, idxCorr]

        # working with the same DAUID
        DAUID = set(input_data.index.unique())
        DAUID_p = set(labels.index.unique())
        idx_DAUID = DAUID.intersection(DAUID_p)

        input_data = input_data.loc[list(idx_DAUID), :]
        labels = labels.loc[list(idx_DAUID), :]

        # CALLING METHODS
        # print("ours\n")
        # ours_score = my_automl(input_data, labels)
        # ours_score_final = ours_score[['R-squared_2', 'Standard_e', 'MSE', 'RMSE']]

        # print("H2O - GLM\n")
        # h2o_score = h2o_glm(input_data, labels)
        # h2o_score_final = h2o_score[['R-squared_2', 'Standard_e', 'MSE', 'RMSE']]

        print("TPOT - AUTOML\n")
        tpot_score, tpot_elapsed, tpot_best_pipeline, tpot_model = tpot_automl(input_data, labels, ps=5, gn=5)
        tpot_score_final = tpot_score[['R-squared_2', 'Standard_e', 'MSE', 'RMSE']]

        # all_scores = pd.concat([h2o_score_final, ours_score_final, tpot_score_final])
        all_scores = pd.concat([all_scores, tpot_score_final])
        all_time = pd.concat([all_time, pd.DataFrame({"Processing Time": [tpot_elapsed]}, columns=["Processing Time"])])

    all_scores.to_csv("results_tpot/all_scores.csv")
    mean_results = all_scores.mean(axis=0)
    mean_results.to_csv("results_tpot/mean_results.csv")
    std_results = all_scores.std(axis=0)
    std_results.to_csv("results_tpot/std_results.csv")

    all_time.to_csv("results_tpo/all_time.csv")
    mean_time = all_time.mean(axis=0)
    mean_time.to_csv("results_tpot/mean_time.csv")
    std_time = all_time.std(axis=0)
    std_time.to_csv("results_tpot/std_time.csv")

    return all_scores, mean_results, std_results, all_time, mean_time, std_time


all_scores, mean_results, std_results, all_time, mean_time, std_time = analyses_models(5, 7, 3)

################
# POLICE DATA
################
# parameters
# number_of_variables_to_index = 3
# number_of_variables_to_correlate = 5

# getting police data frequency and severities
# mp = ml.get_matrix()
# all_data_p = pd.crosstab(index=mp.dauid, columns=mp.ucr_description, values=mp.count_1, aggfunc='sum').fillna(0)
# w = pd.crosstab(index=mp.dauid, columns=mp.ucr_description, values=mp.severity_weight, aggfunc='sum').fillna(0)
# datetime object containing current date and time to be used as seed
# now = timeit.default_timer()
# rng = np.random.RandomState(int(now))
# selecting random variables
# idxCat = np.sort(rng.choice(all_data_p.shape[1], number_of_variables_to_index, replace=False), axis=0)
# creating index
# labels = np.sum((all_data_p.iloc[:, idxCat] * w.iloc[:, idxCat]), axis=1)
# labels = (labels / labels.max())  # normalizing
# labels = pd.DataFrame(labels.to_frame())
# labels = labels.rename(columns={0: 'output'})

################
# DEMOGRAPHIC DATA
################
# m = ml.get_demographic_matrix()
# all_data = pd.crosstab(index=m.dauid, columns=m.attribute, values=m.total, aggfunc='sum').fillna(0)
# selecting random variables
# idxCat = np.sort(rng.choice(all_data.shape[1], number_of_variables_to_correlate, replace=False), axis=0)
# input_data = all_data.iloc[:, idxCat]
# removing missing values
# input_data.replace(to_replace=-1, value=np.nan, inplace=True)
# input_data = input_data.dropna()

# working with the same DAUID
# DAUID = set(input_data.index.unique())
# DAUID_p = set(labels.index.unique())
# idx_DAUID = DAUID.intersection(DAUID_p)

# input_data = input_data.loc[list(idx_DAUID), :]
# labels = labels.loc[list(idx_DAUID), :]

# CALLING METHODS
# print("ours\n")
# ours_score = my_automl(input_data, labels)
# ours_score_final = ours_score[['R-squared_2', 'Standard_e', 'MSE', 'RMSE']]

# print("H2O - GLM\n")
# h2o_score = h2o_glm(input_data, labels)
# h2o_score_final = h2o_score[['R-squared_2', 'Standard_e', 'MSE', 'RMSE']]

# print("TPOT - AUTOML\n")
# tpot_score = tpot_automl(input_data, labels)
# tpot_score_final = tpot_score[['R-squared_2', 'Standard_e', 'MSE', 'RMSE']]

# all_scores = pd.concat([h2o_score_final, ours_score_final, tpot_score_final])
