from lime import lime_tabular

def explainer(input_data):
    return lime_tabular.LimeTabularExplainer(input_data.as_matrix(),
                                             mode='regression',
                                             kernel_width=3,
                                             verbose=True,
                                             feature_names=input_data.columns)

def explain(exp, input_data, model):
    return exp.explain_instance(input_data, model.predict,
                                num_features=5,
                                top_labels=1)



def explain_lime():
    pass

def explain_shap():
    pass