from sklearn import linear_model
from sklearn.linear_model import HuberRegressor, LinearRegression, LogisticRegression, OrthogonalMatchingPursuit

from sklearn.tree import DecisionTreeRegressor
from sklearn.ensemble import RandomForestRegressor
from sklearn.preprocessing import PolynomialFeatures
import xgboost as xgb

import statsmodels.api as sm

from sklearn import preprocessing

import pandas as pd
import numpy as np

########################
# SCORES
########################
# prediction of everything
def prediction(model, x):
    y_pred = model.predict(x)
    return y_pred.ravel()


# computing R-squared and adjusted R squared
def R_score(y, y_p, k):
    y_mean = float(y.mean())
    sq_tot = ((y-y_mean) ** 2).sum()
    sq_res = ((y - y_p) ** 2).sum()
    sq_exp = ((y_p - y_mean) ** 2).sum()

    R2 = sq_exp / sq_tot
    R2_norm = sq_exp / (sq_exp + sq_res)
    R2_2 = 1 - (sq_res / sq_tot)
    adjusted_R = 1 - ((y.shape[0] - 1) / (y.shape[0] - (k + 1))) * (1 - R2)
    Standard_e = np.sqrt(((1/(len(y)-k-1))*sq_res))
    MSE = sq_res/len(y)
    RMSE = np.sqrt(MSE)

    all_r = pd.DataFrame({'sq_tot': [sq_tot], 'sq_res': [sq_res], 'sq_exp': [sq_exp],
                          'R-squared': [R2], 'R-squared_norm': [R2_norm], 'R-squared_2': [R2_2], 'adjustedR': [adjusted_R],
                          'Standard_e': [Standard_e], 'MSE': [MSE], 'RMSE': [RMSE]},
                         columns=['sq_tot', 'sq_res', 'sq_exp',
                                  'R-squared', 'R-squared_norm', 'R-squared_2', 'adjustedR', 'Standard_e', 'MSE', 'RMSE'])

    return all_r

########################
# Linear Regression model
# Two libraries can be used
#    - sklearn
#    - statsmodels (it has paper references)
########################
# Linear regression model using sklearn library
def sk_linear_regression_model(x, y, reshape_label=False, polynomial_degree=1, bias=False):
    print("\t\t linear")
    # reshaping labels between -1 and 1
    if reshape_label:
        y = y.reshape((-1, 1))

    # applying a polynomial kernel on data
    if polynomial_degree > 1:
        x = PolynomialFeatures(degree=polynomial_degree, include_bias=bias).fit_transform(x)

    # model = LinearRegression()
    # model.fit(x, y)
    model = LinearRegression().fit(x, y)
    r_sq = model.score(x, y)

    # print('coefficient of determination:', r_sq)
    # print('intercept:', model.intercept_)
    # print('slope:', model.coef_)

    # how to predict and the math used
    # prediction = intercept_b + x*coef
    # y_pred = model.predict(x)
    # print('predicted response:', y_pred, sep='\n')

    return model


# MARTHA: I am thinking to start with this algorithm,
# because it give more information about the model.
#
# Linear regression model using statsmodels library
#
# It seems to used OLS as distance to be minimized.
# More information in https://www.statsmodels.org/stable/regression.html
def sm_linear_regression_model(x, y, reshape_label=False, polynomial_degree=1, bias=False):
    # reshaping labels between -1 and 1
    if reshape_label:
        y = y.reshape((-1, 1))

    # applying a polynomial kernel on data
    if polynomial_degree > 1:
        x = PolynomialFeatures(degree=polynomial_degree, include_bias=bias).fit_transform(x)

    # add a column of ones to compute the intercept b (b_0)
    # also required in prediction step
    x = sm.add_constant(x)

    # regression model based on ordinary least squares
    model = sm.OLS(y, x)
    # fit model
    results = model.fit()
    print(results.summary())

    # print('coefficient of determination:', results.rsquared)
    # print('adjusted coefficient of determination:', results.rsquared_adj)
    # print('regression coefficients:', results.params)

    # print('predicted response:', results.fittedvalues, sep='\n')
    # print('predicted response:', results.predict(x), sep='\n')

    return results


# Predict labels using statsmodels model
# def sm_prediction(model, x):
#     x = sm.add_constant(x)
#     y_pred = model.predict(x)
#     return y_pred


########################
# Regression Trees
########################
# Create a model using a regression decision tree model
def regression_tree_model(x, y, depth=5):
    print("\t\t regression tree")
    # create a regressor object
    model = DecisionTreeRegressor(max_depth=depth)
    # fit the regressor with X and Y data
    results = model.fit(x, y)
    return results


# Creating random tree model
def random_tree_regression(x, y, depth=5):
    print("\t\t random tree")
    # model = RandomForestRegressor(max_depth=depth, random_state=0, n_estimators=100, min_samples_leaf=3,
    #                               min_samples_split=3)
    model = RandomForestRegressor(max_depth=depth)
    results = model.fit(x, y)
    return results

# XG Boost
def xgbr(x, y):
    x2 = x
    x2.columns = [i.replace("[", "_h125y153").replace("}", "_318335").replace("<", "_ahgsiadghiuh") for i in x2.columns]

    model = xgb.XGBRegressor(objective="reg:linear", random_state=42)
    results = model.fit(x2, y)
    return results


########################
# Logistic Regression
########################
# create a model with logistic regression
def logistic_regression_model(x, y, rs=0, solver_method='lbfgs', multiclass='multinomial'):
    print("\t\t logistic")
    lab_enc = preprocessing.LabelEncoder()
    y2 = lab_enc.fit_transform(y)
    model = LogisticRegression(random_state=rs, solver=solver_method, multi_class=multiclass)
    results = model.fit(x, y2)

    return results

########################
### OTHERS
########################
#LARS
def lars(x, y):
    print("\t\t lars")
    model = linear_model.Lars(n_nonzero_coefs=1)
    results = model.fit(x, y)
    return results

#LARS lasso
def lasso_lars_regression(x, y):
    print("\t\t lasso lars")
    model = linear_model.LassoLars(normalize=False)
    results = model.fit(x, y)
    return results

#
def sgd_regression(x, y):
    print("\t\t sgd")
    model = linear_model.SGDRegressor(tol=1e-3, learning_rate='adaptive')
    results = model.fit(x, y)
    return results

# Orthogonal Matching Pursuit model (OMP)
def omp(x, y):
    print("\t\t omp")
    model = OrthogonalMatchingPursuit()
    results = model.fit(x, y)
    return results

########################
# probabilistic model with spherical Gaussian
########################
# Bayesian Ridge Regression
def bayesian_ridge_regression(x, y):
    print("\t\t bayesian ridge")
    model = linear_model.BayesianRidge()
    results = model.fit(x, y)
    return results

# Automatic Relevance Determination
def ard_regression(x, y):
    print("\t\t ard")
    model = linear_model.ARDRegression()
    results = model.fit(x, y)
    return results


def passive_aggressive_regression(x, y):
    print("\t\t passive aggressive")
    model = linear_model.PassiveAggressiveRegressor()
    results = model.fit(x, y)
    return results

########################
# Ordinary Least Squares
########################
#Theoretical considerations
#Theil-Sen
def theil_sen_regression(x, y):
    print("\t\t theil sen")
    model = linear_model.TheilSenRegressor()
    results = model.fit(x, y)
    return results

# Robust linear model estimation using RANSAC
def ransac(x, y):
    print("\t\t ransac")
    model = linear_model.RANSACRegressor()
    results = model.fit(x, y)
    return results

# Huber Regressor
def huber(x, y):
    print("\t\t huber")
    model = HuberRegressor()
    results = model.fit(x, y)
    return results
