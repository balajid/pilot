import numpy as np
import regression as rg
import pandas as pd
import matplotlib.pyplot as plt
import mlconnector as ml
from datetime import datetime  # take time for random seed
import timeit
from sklearn.preprocessing import StandardScaler


###############
# PREPARING OBJECT WITH MODELS
###############
# Contains models and results
class Models:
    # Contain all models produced
    def __init__(self, x, y):
        self.linear_regression = rg.sk_linear_regression_model(x, y)
        # self.polynomial_regression = rg.sk_linear_regression_model(x, labels, polynomial_degree=2)
        self.decision_tree = rg.regression_tree_model(x, y)
        self.random_forest = rg.random_tree_regression(x, y)
        self.xgbr = rg.xgbr(x, y)
        self.logistic_regression = rg.logistic_regression_model(x, y)
        self.ard = rg.ard_regression(x, y)
        self.lars = rg.lars(x, y)
        self.lasso_lars = rg.lasso_lars_regression(x, y)
        # self.sgd = rg.sgd_regression(x, y)
        self.bayesian_ridge = rg.bayesian_ridge_regression(x, y)
        self.passive_aggressive = rg.passive_aggressive_regression(x, y)
        self.theil_sen = rg.theil_sen_regression(x, y)
        # self.ransac = rg.ransac(x, y)
        self.huber = rg.huber(x, y)
        self.omp = rg.omp(x, y)
        # Scores
        self.scores = pd.DataFrame(
            # R-squared
            {'R-squared': [
                rg.R_score(y, rg.prediction(self.linear_regression, x), x.shape[1])['R-squared_2'],
                rg.R_score(y, rg.prediction(self.decision_tree, x), x.shape[1])['R-squared_2'],
                rg.R_score(y, rg.prediction(self.random_forest, x), x.shape[1])['R-squared_2'],
                rg.R_score(y, rg.prediction(self.xgbr, x), x.shape[1])['R-squared_2'],
                rg.R_score(y, rg.prediction(self.logistic_regression, x), x.shape[1])['R-squared_2'],
                rg.R_score(y, rg.prediction(self.ard, x), x.shape[1])['R-squared_2'],
                rg.R_score(y, rg.prediction(self.lars, x), x.shape[1])['R-squared_2'],
                rg.R_score(y, rg.prediction(self.lasso_lars, x), x.shape[1])['R-squared_2'],
                # rg.R_score(y, rg.prediction(self.sgd, x), x.shape[1])['R-squared_2'],
                rg.R_score(y, rg.prediction(self.bayesian_ridge, x), x.shape[1])['R-squared_2'],
                rg.R_score(y, rg.prediction(self.passive_aggressive, x), x.shape[1])['R-squared_2'],
                rg.R_score(y, rg.prediction(self.theil_sen, x), x.shape[1])['R-squared_2'],
                # rg.R_score(y, rg.prediction(self.ransac, x), x.shape[1])['R-squared_2'],
                rg.R_score(y, rg.prediction(self.huber, x), x.shape[1])['R-squared_2'],
                rg.R_score(y, rg.prediction(self.omp, x), x.shape[1])['R-squared_2']],
                # standard error
                'Standard_error': [
                    rg.R_score(y, rg.prediction(self.linear_regression, x), x.shape[1])['Standard_e'],
                    rg.R_score(y, rg.prediction(self.decision_tree, x), x.shape[1])['Standard_e'],
                    rg.R_score(y, rg.prediction(self.random_forest, x), x.shape[1])['Standard_e'],
                    rg.R_score(y, rg.prediction(self.xgbr, x), x.shape[1])['Standard_e'],
                    rg.R_score(y, rg.prediction(self.logistic_regression, x), x.shape[1])['Standard_e'],
                    rg.R_score(y, rg.prediction(self.ard, x), x.shape[1])['Standard_e'],
                    rg.R_score(y, rg.prediction(self.lars, x), x.shape[1])['Standard_e'],
                    rg.R_score(y, rg.prediction(self.lasso_lars, x), x.shape[1])['Standard_e'],
                    # rg.R_score(y, rg.prediction(self.sgd, x), x.shape[1])['Standard_e'],
                    rg.R_score(y, rg.prediction(self.bayesian_ridge, x), x.shape[1])['Standard_e'],
                    rg.R_score(y, rg.prediction(self.passive_aggressive, x), x.shape[1])['Standard_e'],
                    rg.R_score(y, rg.prediction(self.theil_sen, x), x.shape[1])['Standard_e'],
                    # rg.R_score(y, rg.prediction(self.ransac, x), x.shape[1])['Standard_e'],
                    rg.R_score(y, rg.prediction(self.huber, x), x.shape[1])['Standard_e'],
                    rg.R_score(y, rg.prediction(self.omp, x), x.shape[1])['Standard_e']]
            }, columns=['R-squared', 'Standard_error'],
            index=['linear_regression', 'decision_tree', 'random_forest', 'xgbr', 'logistic_regression', 'ard', 'lars',
                   'lasso_lars',
                   'bayesian_ridge', 'passive_aggressive', 'theil_sen', 'huber', 'omp'])


### Compute Pearson correlation
def idx_cov(x, y):
    covx = []
    for i in range(x.shape[1]):
        # covx.append(np.cov(x[:, i], y)[0,1])
        covx.append((sum((x[:, i] - x[:, i].mean()) * (y - y.mean()))) / ((x.shape[0] - 1) * x[:, i].std() * y.std()))
    idx = (-np.array(covx)).argsort()

    return idx


### Compute mean and standrd deviation
def mean_sd(x):
    mean = pd.DataFrame(x.mean()).T
    sd = pd.DataFrame(x.std()).T
    res = pd.concat([mean, sd], axis=0)

    return res


###############
# K-FOLD CROSS VALIDATION
###############
def k_fold_cross_validation(x, y, k=10):
    print("\t\t k-fold running...")
    nfold = int(x.shape[0] / k)
    idx = list(x.index)
    np.random.shuffle(idx)
    p = 0
    results = pd.DataFrame()
    error = pd.DataFrame()
    results_all = pd.DataFrame()

    count = 1
    while p < x.shape[0]:
        print("\t\t\t\t", count, " out ", k)
        if (x.shape[0] - (p + nfold)) < nfold:
            nfold = x.shape[0] - p
        idx2 = idx[p:p + nfold]

        # test fold
        test = x.loc[idx2, :]
        testLabels = y.loc[idx2]

        # train fold
        train = x.drop(idx2, axis=0)
        trainLabels = y.drop(idx2)

        # creating models and prediction
        mymodels = Models(train.to_numpy(), np.ravel(trainLabels.to_numpy()))
        # prediction = PredictValues(mymodels, test.to_numpy(), np.ravel(testLabels.to_numpy()))

        # results = pd.concat([results, prediction.scores], axis=0, ignore_index=True)
        # all_score = rg.R_score_all(testLabels, prediction, x.shape[1])
        results = pd.concat([results, pd.DataFrame(mymodels.scores['R-squared_2']).T], axis=0, ignore_index=True)
        error = pd.concat([error, pd.DataFrame(mymodels.scores['Standard_e']).T], axis=0, ignore_index=True)
        p = p + nfold
        count = count + 1

    res = mean_sd(results)
    res.index = ['Mean_score', 'SD_score']
    results_all = pd.concat([results_all, res], axis=0)

    res = mean_sd(error)
    res.index = ['Mean_error', 'SD_error']
    results_all = pd.concat([results_all, res], axis=0)

    return results_all


###############
# STATISTICAL VALIDATION
###############
def run_experiments(number_of_varibles_to_index=3, number_of_variables_to_correlate=3, iter=30, plotboxplot=False,
                    kfold=False):
    results_score_test = pd.DataFrame()
    results_score_train = pd.DataFrame()
    results_sd_e_test = pd.DataFrame()
    results_sd_e_train = pd.DataFrame()

    # getting police data frequency and severities
    m = ml.get_matrix()
    all_data = pd.crosstab(index=m.dauid, columns=m.ucr_description, values=m.count_1, aggfunc='sum').fillna(0)
    w = pd.crosstab(index=m.dauid, columns=m.ucr_description, values=m.severity_weight, aggfunc='sum').fillna(0)

    # datetime object containing current date and time to be used as seed
    now = datetime.now()
    rng = np.random.RandomState(now.minute)

    for i in range(iter):
        print("Iteration: ", i)

        # selecting random variables
        idxCat = np.sort(rng.choice(all_data.shape[1], number_of_varibles_to_index, replace=False), axis=0)
        # creating index
        labels = np.sum((all_data.iloc[:, idxCat] * w.iloc[:, idxCat]), axis=1)
        labels = labels / labels.max()  # normalizing

        # Centralizing data
        # scaler = StandardScaler()
        # all_data = scaler.transform(all_data)
        all_data = all_data - all_data.mean()

        # Divinding data between train and test
        data_index = all_data.iloc[:, idxCat]
        remain_data = all_data.drop(all_data.columns[idxCat], axis=1)
        # selecting correlated variables to produce model disconsidering the varibles used to produce the index
        idxCorr = idx_cov(remain_data.to_numpy(), labels.to_numpy())[0:number_of_variables_to_correlate]
        # idxCorr = np.sort(rng.choice(list(set(range(remain_data.shape[1])) - set(idxCat)), 3, False), axis=0)
        input_data = remain_data.iloc[:, idxCorr]

        # print information
        print("Columns for index: ", data_index.columns)
        print("Columns for model: ", input_data.columns)

        ## Select if conduct a k-fold cross-validation or run with all examples
        if kfold == True:
            # k-fold
            # train data
            result = k_fold_cross_validation(data_index, labels)
            results_score_train = pd.concat([results_score_train, pd.DataFrame(result.loc['Mean_score']).T], axis=0,
                                            ignore_index=True)
            results_sd_e_train = pd.concat([results_sd_e_train, pd.DataFrame(result.loc['Mean_error']).T], axis=0,
                                           ignore_index=True)

            # test data
            result = k_fold_cross_validation(input_data, labels)
            results_score_test = pd.concat([results_score_test, pd.DataFrame(result.loc['Mean_score']).T], axis=0,
                                           ignore_index=True)
            results_sd_e_test = pd.concat([results_sd_e_test, pd.DataFrame(result.loc['Mean_error']).T], axis=0,
                                          ignore_index=True)
        else:
            # run model with all
            # train data
            mymodels = Models(data_index, labels)
            results_score_train = pd.concat([results_score_train, pd.DataFrame(mymodels.scores['R-squared']).T],
                                            axis=0, ignore_index=True)
            results_sd_e_train = pd.concat([results_sd_e_train, pd.DataFrame(mymodels.scores['Standard_error']).T],
                                           axis=0, ignore_index=True)

            # test data
            mymodels = Models(input_data, labels)
            results_score_test = pd.concat([results_score_test, pd.DataFrame(mymodels.scores['R-squared']).T], axis=0,
                                           ignore_index=True)
            results_sd_e_test = pd.concat([results_sd_e_test, pd.DataFrame(mymodels.scores['Standard_error']).T],
                                          axis=0,
                                          ignore_index=True)

    ## Generate boxplot figures
    if plotboxplot == True:
        # plot a box plot train
        results_score_train.plot.box(title='Train Score')
        plt.show()
        # plot a box plot test
        results_score_test.plot.box(title='Test Score')
        plt.show()
        # plot a box plot train
        results_sd_e_train.plot.box(title='Train Standard Error')
        plt.show()
        # plot a box plot test
        results_sd_e_test.plot.box(title='Test Standard Error')
        plt.show()

    results_all = pd.DataFrame()

    res = mean_sd(results_score_train)
    res.index = ['Mean_score_train', 'SD_score_train']
    results_all = pd.concat([results_all, res], axis=0)

    res = mean_sd(results_sd_e_train)
    res.index = ['Mean_sd_e_train', 'SD_sd_e_train']
    results_all = pd.concat([results_all, res], axis=0)

    res = mean_sd(results_score_test)
    res.index = ['Mean_score_test', 'SD_score_test']
    results_all = pd.concat([results_all, res], axis=0)

    res = mean_sd(results_sd_e_test)
    res.index = ['Mean_sd_e_test', 'SD_sd_e_test']
    results_all = pd.concat([results_all, res], axis=0)

    return results_all


###############
# MAIN FUNCTION
###############
def compute_ml_regression(indexes, variables):
    # Get matrix as input
    m = ml.get_matrix()
    all_data = pd.crosstab(index=m.dauid, columns=m.ucr_description, values=m.count_1, aggfunc='sum').fillna(0)

    # selecting correlated variables
    input_data_noriginal = all_data.iloc[:, variables]

    # Running model
    all_models = Models(input_data_noriginal, indexes)
    all_prediction = PredictValues(all_models, input_data_noriginal.to_numpy(), np.ravel(indexes.to_numpy()))
    all_scores = rg.R_score_all(indexes, all_prediction, input_data_noriginal.shape[1])
    print(all_scores)

    return all_models

# statistical test
res = run_experiments(5, 5, iter=100)

train_result = res.iloc[[0, 1], :]
train_result.index = ['Mean', 'SD']
test_result = res.iloc[[4, 5], :]
test_result.index = ['Mean', 'SD']

train_result.T.plot.bar(title='Scores of models for train data', ylim=[-1, 1])
plt.show()
test_result.T.plot.bar(title='Scores of models for test data', ylim=[-1, 1])
plt.show()

print(res)
