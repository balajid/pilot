import mlconnector as ml
import numpy as np
import pandas as pd
# from sklearn.decomposition import PCA
import timeit


### Compute Pearson correlation
def idx_cov(x):
    x = x.to_numpy()
    corr = np.zeros((x.shape[1], x.shape[1]))
    for i in range(x.shape[1]):
        for j in range(x.shape[1]):
            cov = (sum((x[:, i] - x[:, i].mean()) * (x[:, j] - x[:, j].mean()))) / x.shape[0]
            corr[i, j] = cov / (x[:, i].std() * x[:, j].std())

    return corr

def idx_cov_2(x):
    # x = x.to_numpy()
    corr = np.zeros((x.shape[1], x.shape[1]))
    for i in range(x.shape[1]):
        for j in range(x.shape[1]):
            cov = (sum((x[:, i] - x[:, i].mean()) * (x[:, j] - x[:, j].mean()))) / x.shape[0]
            corr[i, j] = cov / (x[:, i].std() * x[:, j].std())

    return corr


def norm_mean_deviation(x, std=False):
    matrix = x - x.mean(axis=0)
    if std:
        matrix = np.nan_to_num(matrix / x.std(axis=0))

    return matrix


def norm_0_1(x):
    x_norm = (x - x.min()) / (x.max() - x.min())
    return x_norm


def my_pca(x, kfactor=False):
    cov = np.dot(x.T, x)
    eigvals, eigvecs = np.linalg.eig(cov)
    alpha = eigvals / eigvals.sum()
    if kfactor:
        alpha = alpha[alpha > 0.01]
    k = alpha.shape[0]
    x_rotate = np.dot(x, eigvecs[:, :k])
    I = norm_0_1(np.dot(x_rotate, alpha))

    return I

# number of indicators
number_of_variables_to_index = 10

# reading HRPC
mp = ml.get_matrix()
subcategories = np.unique(mp.ucr)
# all_data_p = pd.crosstab(index=mp.dauid, columns=mp.ucr_description, values=mp.count_1, aggfunc='sum').fillna(0)
all_data_p = pd.crosstab(index=mp.dauid, columns=mp.ucr, values=mp.count_1, aggfunc='sum').fillna(0)

# select the range
numbers_cat = all_data_p.columns[all_data_p.columns < 4000]
numbers_cat2 = all_data_p.columns[all_data_p.columns >= 4000]

# get the severity index
mp['severity_weight'].fillna(np.e, inplace=True)
severity_values = pd.pivot_table(mp, values='severity_weight', index="ucr", aggfunc=np.unique)
severity_weights = np.log(severity_values)

# datetime object containing current date and time to be used as seed
now = timeit.default_timer()
rng = np.random.RandomState(int(now))

iter = 500
average = np.zeros((5, 5))
average_ind = np.zeros((number_of_variables_to_index, number_of_variables_to_index))
standard_deviation = np.zeros((iter, 5, 5))
standard_deviation_ind = np.zeros((iter, number_of_variables_to_index, number_of_variables_to_index))
all_columns = pd.DataFrame()

for i in range(iter):
    ### Selecting categories randonly
    # idxCat = np.sort(rng.choice(all_data_p.shape[1], number_of_variables_to_index, replace=False), axis=0)
    nv = int(np.floor(number_of_variables_to_index * .6))
    idxCat = np.sort(rng.choice(numbers_cat, nv, replace=False), axis=0)
    idxCat = np.hstack(
        [idxCat, np.sort(rng.choice(numbers_cat2, (number_of_variables_to_index - nv), replace=False), axis=0)])
    # idxCat = [3365.0, 4120.0, 4330.0, 1410.0, 4240.0, 6900.0] # correlated
    # idxCat = [1510.0, 4330.0, 6510.0, 3560.0, 9450.0, 9510.0] # not correlated
    # idxCat = [1371.0, 3115.0, 3740.0, 4110.0, 9520.0] # not correlated
    # idxCat = [2153.0, 3410.0, 3490.0, 4130.0, 4220.0] # correlated

    ### Selecting categories
    X = all_data_p.loc[:, idxCat].to_numpy()
    # X = np.power(all_data_p.loc[:, idxCat].to_numpy(), 0.2)
    # X[X == np.inf] = 0
    w = severity_weights.loc[idxCat].to_numpy()
    X_w = X * w.T
    all_columns = pd.concat([all_columns, pd.DataFrame({"Indicators": [idxCat.tolist()]},
                                                       columns=["Indicators"])], ignore_index=True)

    # test data
    # number_of_variables_to_index = 2
    # X = np.asarray([rng.choice(10000, 10), rng.choice(10000, 10)]).T
    # w = np.asarray(rng.choice(10000, 2))
    # X_w = X*w.T

    # creating index our method
    labels = norm_0_1(X_w.sum(axis=1))

    ###
    # PCA without severity
    pca = my_pca(norm_mean_deviation(X, std=False), kfactor=False)

    # PCA with severity
    pca_w = my_pca(norm_mean_deviation(X_w, std=False), kfactor=False)

    ###
    # PCA without severity k-factor
    pca_k = my_pca(norm_mean_deviation(X, std=False), kfactor=True)

    # PCA with severity k-factor
    pca_w_k = my_pca(norm_mean_deviation(X_w, std=False), kfactor=True)

    all_results = pd.DataFrame({"linear_combination": labels,
                                "pca": pca,
                                "pca_weights": pca_w,
                                "pca_kfactor": pca_k,
                                "pca_weights_kfactor": pca_w_k
                                }, columns=["linear_combination", "pca", "pca_weights", "pca_kfactor",
                                            "pca_weights_kfactor"])

    corr = idx_cov(all_results)
    average = average + corr
    standard_deviation[i] = corr

    corr_idx = idx_cov_2(X)
    average_ind = average_ind + corr_idx
    standard_deviation_ind[i] = corr_idx

average = average / iter
average_ind = average_ind / iter

std_2 = np.zeros((5, 5))
std_idx_2 = np.zeros((number_of_variables_to_index, number_of_variables_to_index))
for i in range(iter):
    std_2 = std_2 + (standard_deviation[i]-average)**2
    std_idx_2 = std_idx_2 + (standard_deviation_ind[i]-average_ind)**2

std_2 = np.sqrt(std_2/(iter-1))
