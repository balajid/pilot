import mlconnector as ml
import regression as rg
import numpy as np
import pandas as pd
import h2o
from h2o.estimators.glm import H2OGeneralizedLinearEstimator
from tpot import TPOTRegressor
import timeit


###############
# PREPARING OBJECT WITH MODELS
###############
# Contains models and results
class Models:
    # Contain all models produced
    def __init__(self, x, y):
        self.linear_regression = rg.sk_linear_regression_model(x, y)
        # self.polynomial_regression = rg.sk_linear_regression_model(x, labels, polynomial_degree=2)
        self.decision_tree = rg.regression_tree_model(x, y)
        self.random_forest = rg.random_tree_regression(x, y)
        self.xgbr = rg.xgbr(x, y)
        self.logistic_regression = rg.logistic_regression_model(x, y)
        self.ard = rg.ard_regression(x, y)
        self.lars = rg.lars(x, y)
        self.lasso_lars = rg.lasso_lars_regression(x, y)
        # self.sgd = rg.sgd_regression(x, y)
        self.bayesian_ridge = rg.bayesian_ridge_regression(x, y)
        self.passive_aggressive = rg.passive_aggressive_regression(x, y)
        self.theil_sen = rg.theil_sen_regression(x, y)
        # self.ransac = rg.ransac(x, y)
        self.huber = rg.huber(x, y)
        self.omp = rg.omp(x, y)
        # Scores
        self.scores = pd.DataFrame()
        my_score = rg.R_score(y, rg.prediction(self.linear_regression, x), x.shape[1])
        self.scores = pd.concat([self.scores, my_score], ignore_index=True)
        my_score = rg.R_score(y, rg.prediction(self.decision_tree, x), x.shape[1])
        self.scores = pd.concat([self.scores, my_score], ignore_index=True)
        my_score = rg.R_score(y, rg.prediction(self.random_forest, x), x.shape[1])
        self.scores = pd.concat([self.scores, my_score], ignore_index=True)
        my_score = rg.R_score(y, rg.prediction(self.xgbr, x), x.shape[1])
        self.scores = pd.concat([self.scores, my_score], ignore_index=True)
        my_score = rg.R_score(y, rg.prediction(self.logistic_regression, x), x.shape[1])
        self.scores = pd.concat([self.scores, my_score], ignore_index=True)
        my_score = rg.R_score(y, rg.prediction(self.ard, x), x.shape[1])
        self.scores = pd.concat([self.scores, my_score], ignore_index=True)
        my_score = rg.R_score(y, rg.prediction(self.lars, x), x.shape[1])
        self.scores = pd.concat([self.scores, my_score], ignore_index=True)
        my_score = rg.R_score(y, rg.prediction(self.lasso_lars, x), x.shape[1])
        self.scores = pd.concat([self.scores, my_score], ignore_index=True)
        # my_score = rg.R_score(y, rg.prediction(self.sgd, x), x.shape[1])
        # self.scores = pd.concat([self.scores, my_score], ignore_index=True)
        my_score = rg.R_score(y, rg.prediction(self.bayesian_ridge, x), x.shape[1])
        self.scores = pd.concat([self.scores, my_score], ignore_index=True)
        my_score = rg.R_score(y, rg.prediction(self.passive_aggressive, x), x.shape[1])
        self.scores = pd.concat([self.scores, my_score], ignore_index=True)
        my_score = rg.R_score(y, rg.prediction(self.theil_sen, x), x.shape[1])
        self.scores = pd.concat([self.scores, my_score], ignore_index=True)
        # my_score = rg.R_score(y, rg.prediction(self.ransac, x), x.shape[1])
        # self.scores = pd.concat([self.scores, my_score], ignore_index=True)
        my_score = rg.R_score(y, rg.prediction(self.huber, x), x.shape[1])
        self.scores = pd.concat([self.scores, my_score], ignore_index=True)
        my_score = rg.R_score(y, rg.prediction(self.omp, x), x.shape[1])
        self.scores = pd.concat([self.scores, my_score], ignore_index=True)
        self.scores.index = ['linear_regression','decision_tree',
                             'random_forest','xgbr','logistic_regression',
                             'ard','lars','lasso_lars','bayesian_ridge',
                             'passive_aggressive','theil_sen','huber','omp']


### Compute Pearson correlation
def idx_cov(x, y):
    covx = []
    for i in range(x.shape[1]):
        # covx.append(np.cov(x[:, i], y)[0,1])
        covx.append((sum((x[:, i] - x[:, i].mean()) * (y - y.mean()))) / ((x.shape[0] - 1) * x[:, i].std() * y.std()))
    idx = (-np.array(covx)).argsort()

    return idx


### Compute mean and standrd deviation
def mean_sd(x):
    mean = pd.DataFrame(x.mean()).T
    sd = pd.DataFrame(x.std()).T
    res = pd.concat([mean, sd], axis=0)

    return res

###############
# MY AUTOML
###############
def my_automl(x, y):
    y2 = y.to_numpy().ravel()
    # run model with all
    # test data
    start_time = timeit.default_timer()
    mymodels = Models(x, y2)
    elapsed = timeit.default_timer() - start_time
    print(elapsed)
    print(mymodels.scores)

    return mymodels.scores

###############
# AUTOML TPOT
###############
def tpot_automl(x, y):
# Running automl regression
    # instantiate tpot
    start_time = timeit.default_timer()
    tpot = TPOTRegressor(verbosity=2)
    tpot.fit(x, y.to_numpy().ravel())
    tpot_score = tpot.score(x, y.to_numpy().ravel())
    elapsed = timeit.default_timer() - start_time
    best_pipeline = tpot.fitted_pipeline_

    # Compute R-squared
    my_score = rg.R_score(y.to_numpy().ravel(), tpot.predict(x).ravel(), x.shape[1])
    my_score.index = ['tpot']

    print('Scores:', tpot_score)
    print('Scores:', my_score)
    print(best_pipeline)
    print('Times:', elapsed)

    return my_score

#################################
# CLASSIFICATION WITH H2O AUTOML regression GLM
#################################
def h2o_glm(x, y):
    h2o.init()

    # concat input data and labels
    ylabels = y.columns.to_list()[0]
    x = pd.concat([x, y], axis=1)
    xlabels = x.columns.to_list()
    train = h2o.H2OFrame(x)

    family_sets = ['gaussian',
                   # 'binomial','multinomial','ordinal', # y should be categorical
                   # 'quasibinomial',
                   'poisson',
                   'negativebinomial',
                   # 'gamma',
                   'tweedie']

    results = pd.DataFrame()

    for i in family_sets:
        print(i)
        # Set up GLM for regression
        start_time = timeit.default_timer()
        glm = H2OGeneralizedLinearEstimator(family=i, model_id='glm_default')
        # Use .train() to build the model
        glm.train(x=xlabels,
                  y=ylabels,
                  training_frame=train)

        predictions = glm.predict(train)
        y_p = predictions.as_data_frame()
        # Compute R-squared
        my_score = rg.R_score(y.to_numpy().ravel(), y_p.to_numpy().ravel(), x.shape[1])
        results = pd.concat([results, my_score], ignore_index=True)

    results.index = family_sets
    elapsed = timeit.default_timer() - start_time
    print(elapsed)
    print(results)

    return results

print("\n\nMultiple variables examples:\n")
#input_data = np.array([[0, 1], [5, 1], [15, 2], [25, 5], [35, 11], [45, 15], [55, 34], [60, 35]])
# labels = np.array([4, 5, 20, 14, 32, 22, 38, 43])
#input_data = input_data/input_data.max(axis=0)
# random data
rng = np.random.RandomState(1)
input_data = np.sort(rng.rand(100, 2), axis=0)
w = np.array([3, 5])
labels = np.dot(input_data, w) + 4
labels = labels/labels.max()


input_data = pd.DataFrame(input_data, columns=['A', 'B'])
labels = pd.DataFrame(labels, columns=['output'])

print(input_data)
print(labels)

#CALLING METHODS
print("H2O - GLM\n")
h2o_score = h2o_glm(input_data, labels)
h2o_score_final = h2o_score[['R-squared_2', 'Standard_e']]
print("ours\n")
ours_score = my_automl(input_data, labels)
ours_score_final = ours_score[['R-squared_2', 'Standard_e']]
print("TPOT - AUTOML\n")
tpot_score = tpot_automl(input_data, labels)
tpot_score_final = tpot_score[['R-squared_2', 'Standard_e']]

all_scores = pd.concat([h2o_score_final, ours_score_final, tpot_score_final])
